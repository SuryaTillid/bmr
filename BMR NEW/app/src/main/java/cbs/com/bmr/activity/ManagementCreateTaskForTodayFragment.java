package cbs.com.bmr.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Helper.TaskManager;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TaskList;
import cbs.com.bmr.model.TaskScheduler;

/*********************************************************************
 * Created by Barani on 11-06-2019 in TableMateNew
 ***********************************************************************/
public class ManagementCreateTaskForTodayFragment extends DialogFragment implements View.OnClickListener, On_BackPressed, TaskDetails_ClickListener {

    private EditText editTaskDate, editTaskTime, editDescription, edit_customer, edit_location, edit_approved_by,edit_assign_to;
    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private TextInputLayout task_date_layout;
    private long current_date = System.currentTimeMillis();
    private Calendar myCalendar = Calendar.getInstance();
    private String task_date, time_calc, task_type = "0", task_description = "", task_time, task_date_selected, geo_json_addr;
    private TextView text_date_to_display;
    private Context context;
    private Date s_date;
    private String customerID, customerNAME;
    private String employeeID, employeeNAME,transport_mode = "1";
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private DatePickerDialog.OnDateSetListener date_of_task;
    private int comparison;
    private LinearLayout layout_task;
    private Button btn_submit, btn_general, btn_collection, btn_service, btn_sales;
    private Button btn_bus, btn_bike, btn_car, btn_train, btn_flight;
    private int days = 0;
    private Button fab_add_task;
    private LinearLayout l_layout;
    private ArrayList<TaskScheduler> task_list = new ArrayList<>();
    private ArrayList<TaskScheduler> t_list = new ArrayList<>();
    private ArrayList<TaskList> t_summary_list = new ArrayList<>();
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<EmployeeList> employeeList = new ArrayList<>();
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private ArrayList<String> employee_spinner_list = new ArrayList<>();
    private TaskManager taskManager;
    private ConfigurationSettings settings;
    private LinearLayout l1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.management_create_task, container, false);
        context = getActivity();
        taskManager = new TaskManager(context);
        settings = new ConfigurationSettings(context);
        initControls(view);

        //checkForTaskDetails();

        date_of_task = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                getDate();
            }
        };

        datePickerDialog = new DatePickerDialog(context, date_of_task, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        SimpleDateFormat time_date_format = new SimpleDateFormat("yyyy-MM-dd");
        editTaskDate.setText(time_date_format.format(Calendar.getInstance().getTime()));

        edit_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSpinnerForCustomerSelection();
            }
        });

        edit_approved_by.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSpinnerForEmployeeSelection();
            }
        });

        edit_assign_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSpinnerForEmployeeSelection();
            }
        });

        editTaskTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (!TextUtils.isEmpty(task_date)) {
                Calendar mCurrentTime = Calendar.getInstance();
                final int hours = mCurrentTime.get(Calendar.HOUR_OF_DAY);
                final int minute = mCurrentTime.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time_calc = selectedHour + ":" + selectedMinute + ":" + "00";
                        getTime(selectedHour, selectedMinute);
                        int hour = selectedHour % 12;
                        editTaskTime.setText(String.format("%02d:%02d %s", hour == 0 ? 12 : hour,
                                selectedMinute, selectedHour < 12 ? "am" : "pm"));
                           /* if (selectedHour > 12) {
                                ed_reservation_time.setText("" + selectedHour + ":" + selectedMinute);
                            } else {
                                ed_reservation_time.setText("" + selectedHour + ":" + selectedMinute);
                            }*/
                    }
                }, hours, minute, true);
                timePickerDialog.setTitle("Select Time");
                timePickerDialog.show();

                String s = editTaskDate.getText().toString().trim();
                if (TextUtils.isEmpty(s)) {
                    editTaskTime.setFocusable(true);
                    editTaskTime.setError("Please select date");
                } else {
                    editTaskTime.setError(null);
                }
            }
        });
        new GetTask_List().execute();
        new GetCustomerList().execute();
        new GetEmployeeList().execute();

        /*On submit*/
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callTaskListBeforeSubmit();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Task Scheduler");
    }

    /*
     * Employee Spinner List
     * */
    private void callSpinnerForEmployeeSelection() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_employee_list_spinner);

        final Spinner spinner_employee_list = dialog.findViewById(R.id.employee_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, employee_spinner_list);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_employee_list.setAdapter(ad);
        spinner_employee_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                employeeID = employeeList.get(position).getId();
                employeeNAME = employeeList.get(position).getFirstname();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(employeeID)) {
                    populateEmployeeDetail(employeeID);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void populateEmployeeDetail(String employeeID) {
        for (EmployeeList e : employeeList) {
            if (employeeID.equalsIgnoreCase(e.getId())) {
                edit_assign_to.setText(e.getFirstname());
            }
        }
    }

    private void checkForTaskDetails() {
        t_list = taskManager.getTaskList();
        AppLog.write("DAY", "--" + new Gson().toJson(t_list));
        if (t_list.size() > 0) {
            //editTaskDate.setVisibility(View.GONE);
            editTaskDate.setEnabled(false);
            getDate_if_pre_selected(t_list.get(0).getTask_date());
        } else {
            editTaskDate.setVisibility(View.VISIBLE);
            editTaskDate.setEnabled(true);
        }
    }

    private void getDate_if_pre_selected(String task_date) {
        editTaskDate.setText(task_date);
        task_date_selected = task_date;
    }

    /*
     * Spinner- Customer Selection
     * */
    private void callSpinnerForCustomerSelection() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_customer_list_spinner);

        final Spinner spinner_customer_list = dialog.findViewById(R.id.customer_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, customer_list_spinner);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_customer_list.setAdapter(ad);
        spinner_customer_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerID = customerList.get(position).getId();
                customerNAME = customerList.get(position).getFirst_name();
                geo_json_addr = customerList.get(position).getPrimary_geo_json();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(customerID)) {
                    populateCustomerDetails(customerID);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void populateCustomerDetails(String customerID) {
        for (CustomerList c : customerList) {
            if (customerID.equalsIgnoreCase(c.getId())) {
                edit_customer.setText(c.getFirst_name());
                edit_location.setText(c.getAddress1() + ", " + c.getAddress2() + ", " + c.getCity_village() + ", " + c.getState());
            }
        }
    }

    private void callTaskListBeforeSubmit() {
        if (validate()) {
            //task_date_layout.setVisibility(View.GONE);
            l_layout.setVisibility(View.VISIBLE);
            getTaskDetails();
        }
    }

    private void getTaskDetails() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat key_format = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");
        task_date_selected = editTaskDate.getText().toString().trim();

        String timeStamp = key_format.format(calendar.getTime());
        try {
            TaskScheduler t_list = new TaskScheduler();
            t_list.setT_id("t" + timeStamp);
            t_list.setTask_id(settings.getEmployee_ID() + "emp" + timeStamp);
            t_list.setTask_date(task_date_selected);
            t_list.setTask_type(task_type);
            t_list.setCustomer_id(customerID);
            t_list.setCustomer_name(customerNAME);
            t_list.setDescription(task_description);
            t_list.setCreated_date(date_format.format(calendar.getTime()));
            t_list.setEmp_id(settings.getEmployee_ID());
            t_list.setCreated_by_id(settings.getEmployee_ID());
            t_list.setTime(task_date_selected + " " + time_calc);
            t_list.setLocation(edit_location.getText().toString().trim());
            t_list.setGeo_json(geo_json_addr);
            task_list.add(t_list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AppLog.write("T_LIST__", "" + new Gson().toJson(task_list));
        call_(task_list);
    }

    private void call_(ArrayList<TaskScheduler> task_list) {
        for (TaskScheduler t1 : task_list) {
            AppLog.write("task_list--", "--" + new Gson().toJson(t1));
            taskManager.addTasks(t1);
            createTask(t1);
            //clearDetails();
        }
    }

    private void createTask(TaskScheduler t1) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
        String created_date = date_format.format(calendar.getTime());
        String task_id = t1.getTask_id();
        task_date = t1.getTask_date();
        String created_by_id = settings.getEmployee_ID();
        String task_submit = new Gson().toJson(getTaskDetails_list());
        AppLog.write("Tag_1", task_submit);
        new CreateTaskSummaryDetails().execute(task_id, task_date, created_by_id, created_by_id,employeeID, created_date, task_submit);
    }

    private ArrayList<TaskScheduler> getTaskDetails_list() {
        ArrayList<TaskScheduler> task_List = new ArrayList<>();
        try {
            for (TaskScheduler t : task_list) {
                TaskScheduler t_list = new TaskScheduler();
                t_list.setTask_type(t.getTask_type());
                t_list.setCustomer_id(t.getCustomer_id());
                t_list.setDescription(t.getDescription());
                t_list.setEmp_id(t.getEmp_id());
                t_list.setTime(t.getTime());
                t_list.setLocation(t.getLocation());
                t_list.setGeo_json(t.getGeo_json());
                t_list.setImage("");
                t_list.setTransport(transport_mode);
                task_List.add(t_list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return task_List;
    }

    private void clearDetails() {
        edit_customer.setText("");
        editTaskTime.setText("");
        edit_location.setText("");
        editDescription.setText("");
    }

    private void initControls(View view) {
        editTaskDate = view.findViewById(R.id.edit_task_date);
        editTaskTime = view.findViewById(R.id.edit_task_time);
        editDescription = view.findViewById(R.id.edit_description);
        edit_customer = view.findViewById(R.id.edit_customer);
        edit_location = view.findViewById(R.id.edit_location);
        edit_approved_by = view.findViewById(R.id.edit_approval);
        edit_assign_to = view.findViewById(R.id.edit_assignee);
        layout_task = view.findViewById(R.id.layout_task);
        btn_submit = view.findViewById(R.id.submit_btn);
        fab_add_task = view.findViewById(R.id.fab_task);
        text_date_to_display = view.findViewById(R.id.text_date);
        l_layout = view.findViewById(R.id.l_layout);
        task_date_layout = view.findViewById(R.id.edit_task_date_layout);
        btn_general = view.findViewById(R.id.btn_general);
        btn_collection = view.findViewById(R.id.btn_collection);
        btn_service = view.findViewById(R.id.btn_service);
        btn_sales = view.findViewById(R.id.btn_sales);
        btn_bike = view.findViewById(R.id.btn_bike);
        btn_car = view.findViewById(R.id.btn_car);
        btn_bus = view.findViewById(R.id.btn_bus);
        btn_train = view.findViewById(R.id.btn_train);
        btn_flight = view.findViewById(R.id.btn_flight);

        fab_add_task.setOnClickListener(this);
        btn_general.setOnClickListener(this);
        btn_service.setOnClickListener(this);
        btn_collection.setOnClickListener(this);
        btn_sales.setOnClickListener(this);
        btn_bike.setOnClickListener(this);
        btn_car.setOnClickListener(this);
        btn_bus.setOnClickListener(this);
        btn_train.setOnClickListener(this);
        btn_flight.setOnClickListener(this);
    }

    private void getDate() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");
        editTaskDate.setText(sdf.format(myCalendar.getTime()));
        Date date = new Date(sdf.format(myCalendar.getTime()));
        Date c_date = new Date(sdf.format(current_date));
        AppLog.write("Date", "--" + sdf.format(myCalendar.getTime()) + "----" + sdf1.format(current_date));
        comparison = date.compareTo(c_date);
        AppLog.write("Comparison", "--" + comparison);
        if (comparison == -1) {
            editTaskDate.setError("Please select current or future date");
            editTaskDate.requestFocus();
            Snackbar.make(layout_task, "Please select current or future date..!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        } else {
            editTaskDate.setError(null);
            task_date = sdf1.format(myCalendar.getTime());
            task_date_selected = sdf1.format(myCalendar.getTime());
        }
    }

    private void getTime(int selectedHour, int selectedMinute) {
        task_date = selectedHour + ":" + "0" + selectedMinute + ":" + "00";
        AppLog.write("Time---", task_date);
        time_calc(task_date);
    }

    private void time_calc(String task_date) {
        try {
            s_date = timeFormat.parse(task_date);
            AppLog.write("Values------******-----", "---" + task_date + "---" + time_calc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_task:
                setDate();
                if (validate()) {
                    //task_date_layout.setVisibility(View.GONE);
                    //l_layout.setVisibility(View.VISIBLE);
                    getTaskDetails();
                }
            case R.id.btn_general:
                task_type = "0";
                changeColor(btn_general);
                break;
            case R.id.btn_collection:
                task_type = "1";
                changeColor(btn_collection);
                break;
            case R.id.btn_service:
                task_type = "2";
                changeColor(btn_service);
                break;
            case R.id.btn_sales:
                task_type = "3";
                changeColor(btn_sales);
                break;
            case R.id.btn_bike:
                transport_mode = "1";
                changeColorT(btn_bike);
                break;
            case R.id.btn_car:
                transport_mode = "2";
                changeColorT(btn_car);
                break;
            case R.id.btn_bus:
                transport_mode = "3";
                changeColorT(btn_bus);
                break;
            case R.id.btn_train:
                transport_mode = "4";
                changeColorT(btn_train);
                break;
            case R.id.btn_flight:
                transport_mode = "5";
                changeColorT(btn_flight);
                break;
        }
    }

    private void changeColorT(Button selectedButton) {
        btn_bike.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_car.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_bus.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_train.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_flight.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_bike.setTextColor(Color.parseColor("#4e66f5"));
        btn_car.setTextColor(Color.parseColor("#4e66f5"));
        btn_bus.setTextColor(Color.parseColor("#4e66f5"));
        btn_train.setTextColor(Color.parseColor("#4e66f5"));
        btn_flight.setTextColor(Color.parseColor("#4e66f5"));

        selectedButton.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selectedButton.setTextColor(Color.parseColor("#ffffff"));
    }

    private boolean validate() {
        boolean isValid = true;
        if (editTaskDate.getText().toString().equals("")) {
            editTaskDate.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            editTaskDate.setError(null);
            task_date = editTaskDate.getText().toString().trim();
        }

        if (editTaskTime.getText().toString().equals("")) {
            editTaskTime.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            editTaskTime.setError(null);
            task_time = editTaskTime.getText().toString().trim();
        }

        task_description = editDescription.getText().toString().trim();

        if (edit_customer.getText().toString().equals("")) {
            edit_customer.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_customer.setError(null);
        }

        if (edit_assign_to.getText().toString().equals("")) {
            edit_assign_to.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_assign_to.setError(null);
        }
        return isValid;
    }

    private void changeColor(Button selectedButton) {
        btn_general.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_collection.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_service.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_sales.setBackgroundResource(R.drawable.grey_hollow_round_button);

        btn_general.setTextColor(Color.parseColor("#4e66f5"));
        btn_service.setTextColor(Color.parseColor("#4e66f5"));
        btn_collection.setTextColor(Color.parseColor("#4e66f5"));
        btn_sales.setTextColor(Color.parseColor("#4e66f5"));

        selectedButton.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selectedButton.setTextColor(Color.parseColor("#ffffff"));
    }

    private void setDate() {
        text_date_to_display.setText(editTaskDate.getText().toString().trim());
    }

    /* *
     * On Back Pressed - Call
     * */
    @Override
    public boolean onBackPressed() {
        //getActivity().getSupportFragmentManager().popBackStack();
        callHomeFragment();
        return false;
    }

    private void callHomeFragment() {
        CurrentDateTaskFragment fragment = new CurrentDateTaskFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onTaskClick(int position, String id, String check) {
    }

    @Override
    public void feedbackSubmit(String id, String type) {
    }

    @Override
    public void onClickEdit(int position, String t_id, String task_id) {
    }

    @Override
    public void onClickApproval(int position, String t_id) {
    }

    private class GetTask_List extends AsyncTask<Void, Void, ArrayList<TaskList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TaskList> doInBackground(Void... v) {
            final String url = App.IP_ADDRESS + "/index.php/tasksummary/getlisttaskapi";
            AppLog.write("URL:::", url);
            ArrayList<TaskList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getTaskDetailsList((App) getActivity().getApplication(), settings.getEmployee_ID(),settings.getREGION_ID(),"","");
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<TaskList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            for (TaskList t : list) {
                t_summary_list.add(t);
            }
        }
    }

    private class GetCustomerList extends AsyncTask<Void, Void, ArrayList<CustomerList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<CustomerList> doInBackground(Void... v) {
            ArrayList<CustomerList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCustomerMasterList((App) getActivity().getApplication(),settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<CustomerList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            customer_list_spinner.clear();
            for (CustomerList c_list : list) {
                customerList.add(c_list);
                customer_list_spinner.add(c_list.getFirst_name());
            }
        }
    }

    private class CreateTaskSummaryDetails extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Creating Task...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.createTask((App) getActivity().getApplication(), p[0], p[1], p[2], p[3], p[4], p[5],p[6]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Task Created..!", Toast.LENGTH_SHORT).show();
                    taskManager.clearTasks();
                    callHomeFragment();
                } else {
                    Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetEmployeeList extends AsyncTask<Void, Void, ArrayList<EmployeeList>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeList> doInBackground(Void... v) {
            ArrayList<EmployeeList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getEmployeeList((App) getActivity().getApplication());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            employee_spinner_list.clear();
            for (EmployeeList e_list : list) {
                employeeList.add(e_list);
                employee_spinner_list.add(e_list.getFirstname());
            }
        }
    }
}