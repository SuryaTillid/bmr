package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 03-04-2019 in TableMateNew
 ***********************************************************************/
public class FeedbackDetails {
    private String ques_name;
    private String t_id;
    private String answer;

    public String getQues_name() {
        return ques_name;
    }

    public void setQues_name(String ques_name) {
        this.ques_name = ques_name;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}