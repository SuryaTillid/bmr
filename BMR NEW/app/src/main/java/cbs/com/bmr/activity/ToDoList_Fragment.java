package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.ToDoListener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.ToDoListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.EmployeeInfoForManagementRequest;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TODO_Taskslist;

/*********************************************************************
 * Created by Barani on 25-04-2019 in TableMateNew
 ***********************************************************************/
public class ToDoList_Fragment extends Fragment implements View.OnClickListener, ToDoListener, On_BackPressed {

    private Context context;
    private FloatingActionButton fab_task_create;
    private ToDoListAdapter adapter;
    private String created_by,task_id,task_text,due_date;
    private ConfigurationSettings settings;
    private ArrayList<TODO_Taskslist> taskList = new ArrayList<>();
    private RecyclerView recyclerView_task;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<String> employee_spinner_list = new ArrayList<>();
    private ArrayList<EmployeeList> employeeList = new ArrayList<>();
    private ArrayList<String> department_spinner_list = new ArrayList<>();
    private ArrayList<EmployeeInfoForManagementRequest> deptList = new ArrayList<>();

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_to_do_list, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);

        fab_task_create = view.findViewById(R.id.fab_task_create);
        recyclerView_task = view.findViewById(R.id.rc_TaskDetailsList);
        fab_task_create.setOnClickListener(this);

        adapter = new ToDoListAdapter(context, taskList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setTaskListListener(this);
        recyclerView_task.setLayoutManager(mLayoutManager);
        recyclerView_task.setAdapter(adapter);
        AppLog.write("T1", "" + new Gson().toJson(taskList));

        new GetTaskList().execute();
        new GetEmployeeList().execute();
        new GetDepartmentList().execute();

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fab_task_create:
                openDialogForTaskCreate();
                break;
        }
    }

    private void openDialogForTaskCreate() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_to_do_create);

        Button btn_done = dialog.findViewById(R.id.btn_create_request);
        final EditText e_DueDate = dialog.findViewById(R.id.edit_due_date);
        final EditText edit_task = dialog.findViewById(R.id.edit_task_name);
        final EditText edit_comments = dialog.findViewById(R.id.edit_comments);
        final Button btn_emp_list = dialog.findViewById(R.id.btn_assign_emp);
        final Button btn_dept_list = dialog.findViewById(R.id.btn_assign_dept);

        btn_emp_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callEmployeeListSpinner();
            }
        });

        btn_dept_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDepartmentSpinnerList();
            }
        });

        e_DueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = showDueDateDialog(e_DueDate);
                AppLog.write("TAG_DATE--",""+date);
                e_DueDate.setText(date);
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String task_content = edit_task.getText().toString().trim();
                String comments = edit_comments.getText().toString();

                if(validate(task_content,edit_task))
                {
                    //new CreateTodoTask().execute(task_content, "2019-05-03",comments);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void callDepartmentSpinnerList() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_customer_list_spinner);

        final Spinner spinner_customer_list = dialog.findViewById(R.id.customer_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        TextView t_spinner_header = dialog.findViewById(R.id.t_spinner_header);
        t_spinner_header.setText("Select Department");

        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, department_spinner_list);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_customer_list.setAdapter(ad);
        spinner_customer_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void callEmployeeListSpinner() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_customer_list_spinner);

        final Spinner spinner_customer_list = dialog.findViewById(R.id.customer_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        TextView t_spinner_header = dialog.findViewById(R.id.t_spinner_header);
        t_spinner_header.setText("Select Employee");

        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, employee_spinner_list);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_customer_list.setAdapter(ad);
        spinner_customer_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < employee_spinner_list.size(); i++)
                {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private boolean validate(String task_content, EditText edit_task) {
        boolean isValid = true;
        if (!TextUtils.isEmpty(task_content)) {
            edit_task.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_task.setError(null);
            task_content = edit_task.getText().toString().trim();
        }
        return isValid;
    }

    private void showRepeaterDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_repeat);
        dialog.show();
    }

    private void showReminderDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_remind_me);
        dialog.show();
    }

    private String showDueDateDialog(final EditText e_DueDate) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_due_date);
        Button b_today = dialog.findViewById(R.id.btn_today);
        Button b_tomorrow = dialog.findViewById(R.id.btn_tomorrow);
        Button b_next_week = dialog.findViewById(R.id.btn_next_week);
        Button b_date = dialog.findViewById(R.id.btn_pick_date);

        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat display_date_format = new SimpleDateFormat("dd-MMM-yyyy");

        b_today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                due_date = date_format.format(calendar.getTime());
                AppLog.write("DATE_TODAY----","--"+due_date);
                e_DueDate.setText(due_date);
                dialog.dismiss();
            }
        });

        b_tomorrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String due_d = date_format.format(calendar.getTime());
                try {
                    calendar.setTime(date_format.parse(due_d));
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                due_date = date_format.format(calendar.getTime());
                AppLog.write("DATE_TOMORROW----","--"+due_date);
                e_DueDate.setText(due_date);
                dialog.dismiss();
            }
        });

        b_next_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String due_d = date_format.format(calendar.getTime());
                try {
                    calendar.setTime(date_format.parse(due_d));
                    calendar.add(Calendar.DAY_OF_MONTH, 7);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                due_date = date_format.format(calendar.getTime());
                AppLog.write("DATE_NEXT_WEEK----","--"+due_date);
                e_DueDate.setText(due_date);
                dialog.dismiss();
            }
        });
        b_date.setOnClickListener(this);
        dialog.show();
        return due_date;
    }

    @Override
    public void todoList_click_listener(int position) {
        task_text = taskList.get(position).getTask_name().trim();
        task_id = taskList.get(position).getId();
        openDialogForUpdate(task_text);
    }

    @Override
    public void requestRemoveListener(int id) {
    }

    private void openDialogForUpdate(String task_text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_to_do_list_dialogue);

        Button btnDueDate = dialog.findViewById(R.id.btn_due_date);
        Button btnRemind = dialog.findViewById(R.id.btn_remind);
        Button btnRepeat = dialog.findViewById(R.id.btn_repeat);
        Button btn_done = dialog.findViewById(R.id.btn_done);
        final EditText edit_task = dialog.findViewById(R.id.edit_task);

        edit_task.setText(task_text);

        btnDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showDueDateDialog(edit_task);
            }
        });

        btnRemind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReminderDialog();
            }
        });

        btnRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRepeaterDialog();
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String task_content = edit_task.getText().toString().trim();
                new UpdateTodoTask().execute(task_id,task_content, "2019-05-20", "None");
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class CreateTodoTask extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Creating Task...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.Create_task_todo_list((App) getActivity().getApplication(), p[0], p[1], p[2], settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Task Created..!", Toast.LENGTH_SHORT).show();
                    callListFragment();
                } else {
                    Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetTaskList extends AsyncTask<Void, Void, ArrayList<TODO_Taskslist>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TODO_Taskslist> doInBackground(Void... v) {
            ArrayList<TODO_Taskslist> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getTodo_tasksList((App) getActivity().getApplication());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<TODO_Taskslist> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                taskList.clear();
                for (TODO_Taskslist t : list) {
                    taskList.add(t);
                }
                adapter.MyDataChanged(taskList);
            } else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void callListFragment() {
        new GetTaskList().execute();
    }

    private class UpdateTodoTask extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Creating Task...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.Update_task_todo_list((App) getActivity().getApplication(), p[0], p[1], p[2],p[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Task Updated..!", Toast.LENGTH_SHORT).show();
                    callListFragment();
                } else {
                    Toast.makeText(context, "Failed to update..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to update..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
     * On Back Pressed - Call
     * */
    @Override
    public boolean onBackPressed() {
        return false;
    }

    private class GetEmployeeList extends AsyncTask<Void, Void, ArrayList<EmployeeList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeList> doInBackground(Void... v) {
            ArrayList<EmployeeList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getEmployeeList((App) getActivity().getApplication());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            employee_spinner_list.clear();
            for (EmployeeList e_list : list) {
                employeeList.add(e_list);
                employee_spinner_list.add(e_list.getFirstname());
            }
        }
    }

    private class GetDepartmentList extends AsyncTask<Void, Void, ArrayList<EmployeeInfoForManagementRequest>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeInfoForManagementRequest> doInBackground(Void... v) {
            ArrayList<EmployeeInfoForManagementRequest> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getDepartmentList((App) getActivity().getApplication());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeInfoForManagementRequest> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            department_spinner_list.clear();
            for (EmployeeInfoForManagementRequest d_list : list) {
                deptList.add(d_list);
                department_spinner_list.add(d_list.getDepartment_name());
            }
            AppLog.write("Dept--","-"+new Gson().toJson(department_spinner_list));
        }
    }
}

/*String emp[] = new String[employee_spinner_list.size()];
        for (int i = 0; i < employee_spinner_list.size(); i++) {
            StateSpinner state = new StateSpinner();
            state.setText_value(employee_spinner_list.get(i));
            state.setSelected(false);
            emp[i] = employee_spinner_list.get(i);
        }
        MyAdapter myAdapter = new MyAdapter(getActivity(), 0, emp);
        spinner_customer_list.setAdapter(myAdapter);*/