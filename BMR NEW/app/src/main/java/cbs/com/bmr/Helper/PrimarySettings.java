package cbs.com.bmr.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/*********************************************************************
 * Created by Barani on 16-04-2019 in TableMateNew
 ***********************************************************************/
public class PrimarySettings {

    final String IS_CONFIGURED = "isConfigured";
    final String IS_CHECKED_REMEMBER_ME = "isChecked";
    final String IP_ADDRESS = "ipAddress";
    final String USER_NAME = "user_name";
    final String PASSWORD = "password";
    private final String PREFERENCE_NAME = "BMR_Primary_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;

    public PrimarySettings(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
    }

    public boolean isConfigured() {
        return sPref.getBoolean(IS_CONFIGURED, false);
    }

    public void setIS_CONFIGURED() {
        editor.putBoolean(IS_CONFIGURED, true);
        editor.commit();
    }

    public boolean isChecked() {
        return sPref.getBoolean(IS_CHECKED_REMEMBER_ME, false);
    }

    public void setIS_CHECKED_REMEMBER_ME() {
        editor.putBoolean(IS_CHECKED_REMEMBER_ME, true);
        editor.commit();
    }

    public String getIP_ADDRESS() {
        return sPref.getString(IP_ADDRESS, null);
    }

    public void setIP_ADDRESS(String ipAddress) {
        editor.putString(IP_ADDRESS, ipAddress);
        editor.commit();
    }

    public String getUSER_NAME() {
        return sPref.getString(USER_NAME, null);
    }

    public void setUSER_NAME(String userName) {
        editor.putString(USER_NAME, userName);
        editor.commit();
    }

    public String getPASSWORD() {
        return sPref.getString(PASSWORD, null);
    }

    public void setPASSWORD(String password) {
        editor.putString(PASSWORD, password);
        editor.commit();
    }
}