package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.CustomerNameAdapter;
import cbs.com.bmr.adapter.PondNameListAdapter;
import cbs.com.bmr.adapter.SamplingDataAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.PondList;
import cbs.com.bmr.model.SuccessMessage;

/*********************************************************************
 * Created by Barani on 19-08-2019 in TableMateNew
 ***********************************************************************/
public class SamplingDataList extends Fragment implements On_BackPressed, Pond_details_click_listener {

    private RecyclerView rc_pond_list;
    private SearchableSpinner edit_customer;
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private String customerID, customerNAME,employeeID,harvest_flag ="0";
    private CustomerNameAdapter customerNameAdapter;
    private SamplingDataAdapter adapter;
    private ArrayList<PondList> pondList = new ArrayList<>();
    private ConfigurationSettings settings;
    private LinearLayoutManager mLayoutManager;
    private Context context;
    private Dialog dialog;
    //BottomSheetBehavior sheetBehavior;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pond_sampling, container, false);

        context = getActivity();
        settings = new ConfigurationSettings(context);
        employeeID = settings.getEmployee_ID();

        initialize(view);
        initApiCalls();

        edit_customer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerID = customerList.get(position).getId();
                customerNAME = customerList.get(position).getFirst_name();
                new GetPondSamplingDetails().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapter = new SamplingDataAdapter(context,pondList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setSamplingListListener(this);
        rc_pond_list.setLayoutManager(mLayoutManager);
        rc_pond_list.setAdapter(adapter);

        //sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        /*sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });*/

        return view;
    }

    private void initApiCalls() {
        new GetCustomerList().execute();
    }

    private void initialize(View v) {
        edit_customer = v.findViewById(R.id.edit_customer);
        rc_pond_list = v.findViewById(R.id.rc_pond_list);
    }

    public boolean onBackPressed() {
        callHomeFragment();
        return true;
    }

    private void callHomeFragment() {
        Intent myIntent = new Intent(getActivity(), Home_Page_Activity.class);
        getActivity().startActivity(myIntent);
    }

    @Override
    public void onPondClick(int position, String id, final String cycle_id) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_create_sampling);

        final Button btn_create_sampling = dialog.findViewById(R.id.btn_sample_create);
        final Button btn_sampling = dialog.findViewById(R.id.btn_sampling);
        final Button btn_harvest = dialog.findViewById(R.id.btn_harvest);
        final EditText edit_daily_feed = dialog.findViewById(R.id.edit_daily_feed);
        final EditText edit_abw = dialog.findViewById(R.id.edit_abw);

        btn_create_sampling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String recorded_date = date_format.format(c.getTime());
                String abw = edit_abw.getText().toString().trim();
                String daily_feed = edit_daily_feed.getText().toString().trim();
                if(validate(edit_abw,edit_daily_feed))
                {
                    new CreateSampling_Details().execute(cycle_id,employeeID,daily_feed,recorded_date,harvest_flag,abw);
                }
            }
        });

        btn_sampling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                harvest_flag = "0";
                changeColor(btn_sampling,btn_sampling,btn_harvest);
                btn_create_sampling.setText("CREATE SAMPLING");
            }
        });

        btn_harvest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                harvest_flag = "1";
                changeColor(btn_harvest,btn_sampling,btn_harvest);
                btn_create_sampling.setText("CREATE HARVEST");
            }
        });
        dialog.show();
    }

    private boolean validate(EditText e_abw, EditText e_daily_feed) {
        boolean isValid = true;
        if (e_abw.getText().toString().equals("")) {
            e_abw.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_abw.setError(null);
        }

        if (e_daily_feed.getText().toString().equals("")) {
            e_daily_feed.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_daily_feed.setError(null);
        }
        return isValid;
    }

    private void changeColor(Button selected_btn,Button btn_active,Button btn_harvest) {
        btn_active.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_harvest.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_active.setTextColor(Color.parseColor("#4e66f5"));
        btn_harvest.setTextColor(Color.parseColor("#4e66f5"));

        selected_btn.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_btn.setTextColor(Color.parseColor("#ffffff"));
    }

    private class GetPondSamplingDetails extends AsyncTask<String, String, ArrayList<PondList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<PondList> doInBackground(String... v) {
            ArrayList<PondList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getPONDSamplingDetails((App) getActivity().getApplication(),customerID);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<PondList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                pondList.clear();
                for (PondList pond : list) {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(pond));
                    pondList.add(pond);
                }
                adapter.MyDataChanged(pondList);
            } else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetCustomerList extends AsyncTask<Void, Void, ArrayList<CustomerList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<CustomerList> doInBackground(Void... v) {
            ArrayList<CustomerList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCustomerMasterList((App) getActivity().getApplication(),settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<CustomerList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            customerList.clear();
            customer_list_spinner.clear();
            for (CustomerList c_list : list)
            {
                customerList.add(c_list);
                customer_list_spinner.add(c_list.getFirst_name());
            }
            AppLog.write("CUSTOMER_RECORD--","--"+new Gson().toJson(customerList));
            getCustomerName();
        }
    }

    /* *
     * Get customer record - auto complete
     * */
    private void getCustomerName() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, customer_list_spinner);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edit_customer.setTitle("Select Customer");
        edit_customer.setAdapter(ad);
    }

    private class CreateSampling_Details extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog m_dialog;

        @Override
        protected void onPreExecute() {
            m_dialog = new MyCustomDialog(context, "Creating Task...");
            m_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.createSamplingRecord((App) getActivity().getApplication(),s[0],s[1],s[2],s[3],s[4],s[5]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != m_dialog && m_dialog.isShowing())
                m_dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Sample Created..!", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

