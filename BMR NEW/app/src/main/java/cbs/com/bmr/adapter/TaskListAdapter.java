package cbs.com.bmr.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.model.TaskList;

/*********************************************************************
 * Created by Barani on 26-03-2019 in TableMateNew
 ********************************************************************/
public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.MyHolder> {
    private ArrayList<TaskList> taskLists;
    private Context context;
    private LayoutInflater inflater;
    private TaskDetails_ClickListener listener;

    public TaskListAdapter(Context context, ArrayList<TaskList> taskList) {
        this.context = context;
        this.taskLists = taskList;
        inflater = LayoutInflater.from(context);
    }

    public void setTaskListListener(TaskDetails_ClickListener locationListener) {
        this.listener = locationListener;
    }

    public void MyDataChanged(ArrayList<TaskList> list) {
        taskLists = list;
        notifyDataSetChanged();
    }

    @Override
    public TaskListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_tasklist_modified, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(TaskListAdapter.MyHolder holder, int position) {
        TaskList taskList = taskLists.get(position);

        holder.t_customer_name.setText(taskList.getCustomername());
        holder.t_date.setText(taskList.getTask_date());
        holder.t_type.setText(type_validate(taskList.getTask_type()));
        holder.txt_check_in_time.setText(date_validate(taskList.getChecked_in()));
        holder.txt_check_out_time.setText(date_validate(taskList.getChecked_out()));
        holder.txt_feedback_time.setText(date_validate(taskList.getFb_submit_time()));
        holder.t_approve.setText(approveValidate(taskList.getApproved_status()));
        holder.t_created_by.setText(taskList.getFirst_name());

        String check_in_time = taskList.getChecked_in();
        String check_out_time = taskList.getChecked_out();
        String feedback_time = taskList.getFb_submit_time();

        AppLog.write("TAGS--","-"+check_in_time+"--"+check_out_time+"---"+feedback_time);

        //validate - feedback
        if(!TextUtils.isEmpty(feedback_time)||TextUtils.isEmpty(check_in_time))
        {
            holder.feedback.setAlpha((float) 0.5);
            holder.feedback.setEnabled(false);
        }
        else
        {
            holder.feedback.setAlpha((float) 1);
            holder.feedback.setEnabled(true);
        }

        //validate - check-in
        if(!TextUtils.isEmpty(check_in_time))
        {
            holder.check_in.setAlpha((float) 0.5);
            holder.check_in.setEnabled(false);
        }
        else
        {
            holder.check_in.setAlpha((float) 1);
            holder.check_in.setEnabled(true);
        }

        //validate - check-out
        if(!TextUtils.isEmpty(check_out_time)||TextUtils.isEmpty(check_in_time)||TextUtils.isEmpty(feedback_time))
        {
            holder.check_out.setAlpha((float) 0.5);
            holder.check_out.setEnabled(false);
        }
        else
        {
            holder.check_out.setAlpha((float) 1);
            holder.check_out.setEnabled(true);
        }

        if(!TextUtils.isEmpty(taskList.getLocation()))
        {
            holder.addr_layout.setVisibility(View.VISIBLE);
            holder.t_location_address.setText(taskList.getLocation());
        }else
        {
            holder.addr_layout.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(taskList.getDescription()))
        {
            holder.t_purpose.setText(taskList.getDescription());
        }else
        {
            holder.t_purpose.setText("NONE");
        }
    }

    private String approveValidate(String approved_status) {
        switch(approved_status)
        {
            case "0":
                approved_status = "Pending";
                break;
            case "1":
                approved_status = "Approved";
                break;
            case "2":
                approved_status = "Cancelled";
                break;
        }
        return approved_status;
    }

    private String type_validate(String task_type) {
        switch(task_type)
        {
            case "0":
                task_type = "General";
                break;
            case "1":
                task_type = "Collection";
                break;
            case "2":
                task_type = "Services";
                break;
            case "3":
                task_type = "Sales";
                break;
        }
        return task_type;
    }

    private String date_validate(String task_date) {
        try {
            if(!TextUtils.isEmpty(task_date))
            {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(task_date);
                DateFormat date_formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                task_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return task_date;
    }

    @Override
    public int getItemCount() {
        return taskLists.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView t_location_address;
        TextView t_date;
        TextView t_approve;
        TextView t_purpose,t_customer_name;
        TextView t_edit,t_type;
        TextView t_created_by;
        RelativeLayout r_expand_layout;
        Button check_in,check_out,feedback;
        LinearLayout commentLayout,addr_layout,layout;
        ImageView img_checked_in;
        TextView txt_check_in_time,txt_feedback_time,txt_check_out_time;

        public MyHolder(View v) {
            super(v);
            t_customer_name = v.findViewById(R.id.t_customer_name);
            t_date = v.findViewById(R.id.t_task_date);
            t_edit = v.findViewById(R.id.t_edit);
            r_expand_layout = v.findViewById(R.id.expand_layout);
            t_type = v.findViewById(R.id.txtType);
            t_location_address = v.findViewById(R.id.t_address);
            t_purpose = v.findViewById(R.id.txtPurpose);
            t_created_by = v.findViewById(R.id.t_created_by);
            check_in = v.findViewById(R.id.btn_check_in);
            check_out = v.findViewById(R.id.btn_check_out);
            feedback = v.findViewById(R.id.btn_feedback);
            commentLayout = v.findViewById(R.id.l1);
            addr_layout = v.findViewById(R.id.l_addr);
            t_approve = v.findViewById(R.id.t_approval);
            img_checked_in = v.findViewById(R.id.img_checked_in);
            txt_check_in_time = v.findViewById(R.id.txt_check_in_time);
            txt_check_out_time = v.findViewById(R.id.txt_check_out_time);
            txt_feedback_time = v.findViewById(R.id.txt_feedback_time);
            layout = v.findViewById(R.id.layout_t);

            v.setOnClickListener(new View.OnClickListener() {
                int check = 1;
                @Override
                public void onClick(View v) {
                    if(check == 1)
                    {
                        r_expand_layout.setVisibility(View.VISIBLE);
                        check = 0;
                    }else{
                        r_expand_layout.setVisibility(View.GONE);
                        check = 1;
                    }
                }
            });

            check_in.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String check = "1";
                    listener.onTaskClick(getAdapterPosition(),taskLists.get(getAdapterPosition()).getId(),check);
                    AppLog.write("IN-ID--",taskLists.get(getAdapterPosition()).getId());
                }
            });

            feedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String feedback_time = taskLists.get(getAdapterPosition()).getFb_submit_time();
                    String type = check_for_type(taskLists.get(getAdapterPosition()).getTask_type());
                    if(!TextUtils.isEmpty(feedback_time))
                    {
                        Toast.makeText(context,"Feedback Already Saved.!",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        listener.feedbackSubmit(taskLists.get(getAdapterPosition()).getId(),type);
                    }
                    AppLog.write("ID--",taskLists.get(getAdapterPosition()).getId());
                }
            });

            check_out.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String check = "2";
                    listener.onTaskClick(getAdapterPosition(),taskLists.get(getAdapterPosition()).getId(),check);
                    AppLog.write("OUT-ID--",taskLists.get(getAdapterPosition()).getId());
                    //listener.activateLocation(taskLists.get(getAdapterPosition()).getId());
                }
            });

            t_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if(!taskLists.get(getAdapterPosition()).getApproved_status().equalsIgnoreCase("1"))
                    //{
                        listener.onClickEdit(getAdapterPosition(),taskLists.get(getAdapterPosition()).getId(),taskLists.get(getAdapterPosition()).getTs_id());
                    /*}else
                    {
                        Snackbar.make(layout,"Cant edit this task..!",Snackbar.LENGTH_SHORT).show();
                    }*/
                }
            });

            t_approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickApproval(getAdapterPosition(),taskLists.get(getAdapterPosition()).getId());
                }
            });
        }

        private String check_for_type(String task_type) {
            String s = "";
            switch (task_type)
            {
                case "General":
                    s = "1";
                    break;
                case "Collection":
                    s = "2";
                    break;
                case "Service":
                    s = "3";
                    break;
                case "Sales":
                    s = "4";
                    break;
            }
            return s;
        }
    }
}