package cbs.com.bmr.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.PondDetailedList;
import cbs.com.bmr.model.PondList;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public class PondDetailedListFragment extends Fragment implements On_BackPressed {

    private ConfigurationSettings settings;
    private Context context;
    private TextView t_pond_name, t_wsa, t_seed_stock, t_salinity, t_ph, t_density, t_comments, t_status, t_cycle_status, t_stock_date, t_record_date;
    private TextView t_update;
    private String cycle_id, pond_id;
    private ArrayList<PondList> pondList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pond_details, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);
        initialize(view);

        Bundle bundle = this.getArguments();
        cycle_id = bundle.getString("CYCLE_ID");
        pond_id = bundle.getString("POND_ID");

        /*API for get POND detailed list*/
        new GetPOND_Detailed_List().execute(cycle_id, pond_id);

        t_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             PondUpdateFragment fragment = new PondUpdateFragment();
             if (fragment != null) {
                 FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                 fragmentTransaction.replace(R.id.container_body, fragment);
                 Bundle bundle = new Bundle();
                 bundle.putString("POND_ID", pond_id);
                 bundle.putString("CYCLE_ID", cycle_id);
                 AppLog.write("POND","--"+pond_id+"--"+cycle_id);
                 fragment.setArguments(bundle);
                 fragmentTransaction.commit();
             }
            }
        });
        return view;
    }

    private void initialize(View v) {
        t_pond_name = v.findViewById(R.id.txt_pond_name);
        t_wsa = v.findViewById(R.id.txtWSA);
        t_seed_stock = v.findViewById(R.id.txtSeedStocking);
        t_salinity = v.findViewById(R.id.txt_salinity);
        t_ph = v.findViewById(R.id.txt_ph);
        t_density = v.findViewById(R.id.txtDensity);
        t_comments = v.findViewById(R.id.txtComments);
        t_status = v.findViewById(R.id.txtStatus);
        t_cycle_status = v.findViewById(R.id.txtCycle_Status);
        t_stock_date = v.findViewById(R.id.txtStockingDate);
        t_record_date = v.findViewById(R.id.txtRecordedDate);
        t_update = v.findViewById(R.id.txt_update);
    }

    @Override
    public boolean onBackPressed() {
        callHomeFragment();
        return true;
    }

    private void callHomeFragment()
    {
        PondListFragment fragment = new PondListFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    private class GetPOND_Detailed_List extends AsyncTask<String, String, PondDetailedList> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected PondDetailedList doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            PondDetailedList list = call.getPOND_Details((App) getActivity().getApplication(), s[0], s[1]);
            return list;
        }

        @Override
        protected void onPostExecute(PondDetailedList list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                setPondValues(list);
            } else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setPondValues(PondDetailedList p) {
        t_pond_name.setText(p.getPond_id());
        t_wsa.setText(p.getWSA());
        t_seed_stock.setText(p.getSeed_stocking());
        t_salinity.setText(p.getSalinity());
        t_ph.setText(p.getPh());
        t_comments.setText(p.getComments());
        t_density.setText(p.getDensity());
        t_status.setText(validateStatus(p.getStatus()));
        t_cycle_status.setText(validateCycleStatus(p.getCycle_status()));
        t_stock_date.setText(validateDate(p.getStocking_date()));
        t_record_date.setText(validateDate(p.getRecorded_date()));
    }

    private String validateDate(String stocking_date) {
        try {
            if(!TextUtils.isEmpty(stocking_date))
            {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(stocking_date);
                DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy");
                stocking_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stocking_date;
    }

    private String validateCycleStatus(String cycle_status) {
        if(cycle_status.equalsIgnoreCase("0"))
        {
            cycle_status = "Active";
        }
        else
        {
            cycle_status = "Harvested";
        }
        return cycle_status;
    }

    private String validateStatus(String status) {
        if(status.equalsIgnoreCase("0"))
        {
            status = "Active";
        }
        else
        {
            status = "In Active";
        }
        return status;
    }
}
