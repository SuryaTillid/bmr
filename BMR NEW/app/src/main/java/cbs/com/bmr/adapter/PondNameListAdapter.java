package cbs.com.bmr.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.model.PondList;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public class PondNameListAdapter extends RecyclerView.Adapter<PondNameListAdapter.MyHolder>{

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<PondList> pondList;
    private Pond_details_click_listener listener;

    public PondNameListAdapter(Context context, ArrayList<PondList> pondList) {
        this.context = context;
        this.pondList = pondList;
        inflater = LayoutInflater.from(context);
    }

    public void setPondListListener(Pond_details_click_listener listener) {
        this.listener = listener;
    }

    public void MyDataChanged(ArrayList<PondList> list) {
        pondList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PondNameListAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.pond_list_row, parent, false);
        return new PondNameListAdapter.MyHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PondNameListAdapter.MyHolder holder, int position) {

        PondList list = pondList.get(position);

        holder.t_pond_name.setText(list.getPond_id());

        if(list.getStatus().equalsIgnoreCase("0"))
        {
            holder.layout_frame_active_inactive.setBackgroundColor(Color.parseColor("#008000"));
        }
        else
        {
            holder.layout_frame_active_inactive.setBackgroundColor(Color.parseColor("#e60000"));
        }
    }

    @Override
    public int getItemCount() {
        return pondList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView t_pond_name;
        FrameLayout layout_frame_active_inactive;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            t_pond_name = itemView.findViewById(R.id.t_pond_name);
            layout_frame_active_inactive = itemView.findViewById(R.id.layout_frame_active_inactive);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPondClick(getAdapterPosition(),pondList.get(getAdapterPosition()).getId(),
                            pondList.get(getAdapterPosition()).getCycle_id());
                }
            });
        }
    }
}

