package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.ToDoListener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.RequestListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.ManagementRequestList;
import cbs.com.bmr.model.SuccessMessage;

/*********************************************************************
 * Created by Barani on 09-05-2019 in TableMateNew
 ***********************************************************************/
public class ManagementRequest extends Fragment implements On_BackPressed, ToDoListener {

    private Context context;
    private RecyclerView recyclerView_requestsList;
    private LinearLayoutManager mLayoutManager;
    private ConfigurationSettings settings;
    private RequestListAdapter adapter;
    private FloatingActionButton fab_create_request;
    private ArrayList<ManagementRequestList> m_list = new ArrayList<>();

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_management_request, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);

        recyclerView_requestsList = view.findViewById(R.id.rc_TaskDetailsList);
        fab_create_request = view.findViewById(R.id.fab_request_create);

        adapter = new RequestListAdapter(context,m_list);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setTaskListListener(this);
        recyclerView_requestsList.setLayoutManager(mLayoutManager);
        recyclerView_requestsList.setAdapter(adapter);
        AppLog.write("T1", "--" + new Gson().toJson(m_list));

        new GetRequestList().execute();

        fab_create_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManagementRequestCreate fragment = new ManagementRequestCreate();
                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();
                }
            }
        });

        return view;
    }

    @Override
    public boolean onBackPressed() {
        TaskListFragment fragment = new TaskListFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
        return false;
    }

    @Override
    public void todoList_click_listener(int position) {
        String task_id = m_list.get(position).getId();
        Bundle bundle = new Bundle();
        bundle.putString("TASK_R_ID", task_id);
        ManagementRequestUpdate fragment = new ManagementRequestUpdate();
        fragment.setArguments(bundle);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void requestRemoveListener(int position) {
        final String task_id = m_list.get(position).getId();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to remove this request?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteRequest().execute(task_id);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class GetRequestList extends AsyncTask<Void, Void, ArrayList<ManagementRequestList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<ManagementRequestList> doInBackground(Void... v) {
            ArrayList<ManagementRequestList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getManagementRequestList((App) getActivity().getApplication(),settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<ManagementRequestList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Request_list",""+new Gson().toJson(list));
            if (null != list)
            {
                m_list.clear();
                for (ManagementRequestList m : list) {
                    AppLog.write("Checking 1.", "---------" + new Gson().toJson(m));
                    m_list.add(m);
                }
                adapter.MyDataChanged(m_list);
            }
            else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DeleteRequest extends AsyncTask<String,String, SuccessMessage> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.ManagementRequestDelete((App) getActivity().getApplication(), s[0]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Successfully removed..!", Toast.LENGTH_SHORT).show();
                    new GetRequestList().execute();
                } else {
                    Toast.makeText(getActivity(), "Failed to remove request..!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
