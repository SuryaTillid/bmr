package cbs.com.bmr.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.SuccessMessage;

import static cbs.com.bmr.activity.LoginActivity.isValidPassword;

/*********************************************************************
 * Created by Barani on 27-05-2019 in TableMateNew
 ***********************************************************************/
public class ChangePasswordFragment extends Fragment {

    private Context context;
    private String employeeID, employeeNAME;
    private ArrayList<String> employee_spinner_list = new ArrayList<>();
    private ArrayList<EmployeeList> employeeList = new ArrayList<>();
    private ConfigurationSettings settings;
    private String user_name;
    private EditText e_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_password_change, container, false);

        context = getActivity();
        settings = new ConfigurationSettings(context);

        e_name = v.findViewById(R.id.ed_userName);
        final EditText e_new_password = v.findViewById(R.id.ed_new_password);
        final EditText e_confirm_password = v.findViewById(R.id.ed_confirm_password);
        final EditText e_old_password = v.findViewById(R.id.ed_old_password);
        Button btn_change = v.findViewById(R.id.b_change_pwd);

        new LoadEmployeeRecords().execute();

        e_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callSpinnerForEmployeeSelection(e_name);
            }
        });

        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_password = e_new_password.getText().toString().trim();
                String confirm_password = e_confirm_password.getText().toString().trim();
                String current_password = e_old_password.getText().toString().trim();

                AppLog.write("Password--",""+new_password+"--"+confirm_password);
                if(validatePwd(e_name,e_new_password,e_confirm_password,e_old_password))
                {
                    if(new_password.length()<8 &&!isValidPassword(new_password))
                    {
                        Toast.makeText(context, "Invalid Password", Toast.LENGTH_SHORT).show();
                        e_new_password.setError("Enter valid password");
                    }
                    else if(!new_password.equalsIgnoreCase(confirm_password))
                    {
                        Toast.makeText(context, "Re-check password confirmation..!", Toast.LENGTH_SHORT).show();
                        e_confirm_password.setError("Password Mismatched");
                    }
                    else if(!current_password.equalsIgnoreCase(settings.getPASSWORD()))
                    {
                        Toast.makeText(context, "Current password you have entered is incorrect..!", Toast.LENGTH_SHORT).show();
                        e_old_password.setError("Incorrect Password");
                    }
                    else
                    {
                        AppLog.write("Credits--",""+new_password+"--"+employeeID);
                        e_confirm_password.setError(null);
                        e_old_password.setError(null);
                        e_new_password.setError(null);
                        new ChangePassword().execute(settings.getEmployee_ID(),new_password);
                    }
                }
            }
        });
        return v;
    }

    private void callSpinnerForEmployeeSelection(final EditText e_name) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_employee_list_spinner);

        final Spinner spinner_employee_list = dialog.findViewById(R.id.employee_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, employee_spinner_list);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_employee_list.setAdapter(ad);
        spinner_employee_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                employeeID = employeeList.get(position).getId();
                employeeNAME = employeeList.get(position).getFirstname();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(employeeID))
                {
                    populateEmployeeDetail(employeeID,e_name);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void populateEmployeeDetail(String employeeID, EditText e_name) {
        for (EmployeeList e : employeeList) {
            if (employeeID.equalsIgnoreCase(e.getId())) {
                e_name.setText(e.getFirstname());
            }
        }
    }

    private boolean validatePwd(EditText e_name, EditText e_new_password, EditText e_confirm_password, EditText e_old_password) {
        String e_passWord = e_new_password.getText().toString().trim();
        String e_user = e_name.getText().toString().trim();
        String confirm_password = e_confirm_password.getText().toString().trim();
        if (e_user.equals("")) {
            e_name.setError("Enter Username");
            e_name.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(e_passWord)) {
            e_new_password.setError("Enter Current Password");
            e_new_password.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(e_passWord)) {
            e_new_password.setError("Enter Password");
            e_new_password.requestFocus();
            return false;
        }
        else if (TextUtils.isEmpty(confirm_password)) {
            e_confirm_password.setError("Re-Enter the Password");
            e_confirm_password.requestFocus();
            return false;
        }
        return true;
    }

    public boolean onBackPressed() {
        callHomeFragment();
        return true;
    }

    private class ChangePassword extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_ChangePassword((App) getActivity().getApplication(),p[0],p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if(response!=null)
            {
                if(response.getSuccess().equalsIgnoreCase("1"))
                {
                    Toast.makeText(context,"Password successfully updated..!",Toast.LENGTH_SHORT).show();
                    Logout_screen();
                }
            }
        }
    }

    private void Logout_screen() {
        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);
    }

    private void callHomeFragment() {
        Intent myIntent = new Intent(getActivity(), Home_Page_Activity.class);
        getActivity().startActivity(myIntent);
    }

    private class LoadEmployeeRecords extends AsyncTask<Void, Void, ArrayList<EmployeeList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeList> doInBackground(Void... v) {
            ArrayList<EmployeeList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getEmployeeList((App) getActivity().getApplication());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            employee_spinner_list.clear();
            for (EmployeeList e_list : list) {
                employeeList.add(e_list);
                employee_spinner_list.add(e_list.getFirstname());
                if(settings.getEmployee_ID().equalsIgnoreCase(e_list.getId()))
                {
                    user_name = e_list.getFirstname();
                    e_name.setText(user_name);
                }
            }
        }
    }
}
