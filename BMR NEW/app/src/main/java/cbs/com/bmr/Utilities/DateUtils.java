package cbs.com.bmr.Utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.configuration.AppLog;

/*********************************************************************
 * Created by Barani on 27-03-2019 in TableMateNew
 ***********************************************************************/
public class DateUtils {
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    public static long addDays(int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.DATE, days);
        return cal.getTimeInMillis();
    }

    public static long reduceDays(int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.add(Calendar.DATE, -days);
        return cal.getTimeInMillis();
    }

    public static String getFormatedDate(long dateTime) {
        return dateFormat.format(new Date(dateTime));
    }

    private static String validate(String month) {
        String days = "31";
        switch (month)
        {
            case "1":
            case "3":
            case "5":
            case "7":
            case "8":
            case "10":
            case "12":
                days = "31";
                break;
            case "4":
            case "6":
            case "9":
            case "11":
                days = "30";
                break;
            case "2":
                days = "29";
                break;
        }
        AppLog.write("Date----*","--"+days);
        return days;
    }
}

