package cbs.com.bmr.Helper;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/*********************************************************************
 * Created by Barani on 01-04-2019 in TableMateNew
 ***********************************************************************/
public class TypefaceUtil {
    private static final int REGULAR = 1;
    private static final int BOLD = 2;
    private static final int ITALIC = 3;
    private static final int BOLD_ITALIC = 4;
    private static final int LIGHT = 5;
    private static final int CONDENSED = 6;
    private static final int THIN = 7;
    private static final int MEDIUM = 8;
    private static final String SANS_SERIF = "sans-serif";
    private static final String SANS_SERIF_LIGHT = "sans-serif-light";
    private static final String SANS_SERIF_CONDENSED = "sans-serif-condensed";
    private static final String SANS_SERIF_THIN = "sans-serif-thin";
    private static final String SANS_SERIF_MEDIUM = "sans-serif-medium";
    private static final String FIELD_DEFAULT = "DEFAULT";
    private static final String FIELD_SANS_SERIF = "SANS_SERIF";
    private static final String FIELD_SERIF = "SERIF";
    private static final String FIELD_DEFAULT_BOLD = "DEFAULT_BOLD";

    public TypefaceUtil() {
    }

    public static void overrideFonts(Context context) {
        Typeface regular = getTypeface(1, context);
        Typeface light = getTypeface(1, context);
        Typeface condensed = getTypeface(6, context);
        Typeface thin = getTypeface(7, context);
        Typeface medium = getTypeface(8, context);
        if(Build.VERSION.SDK_INT >= 21) {
            HashMap fonts = new HashMap();
            fonts.put("sans-serif", regular);
            fonts.put("sans-serif-light", light);
            fonts.put("sans-serif-condensed", condensed);
            fonts.put("sans-serif-thin", thin);
            fonts.put("sans-serif-medium", medium);
            overrideFontsMap(fonts);
        } else {
            overrideFont("SANS_SERIF", getTypeface(1, context));
            overrideFont("SERIF", getTypeface(1, context));
        }

        overrideFont("DEFAULT", getTypeface(1, context));
        overrideFont("DEFAULT_BOLD", getTypeface(2, context));
    }

    private static void overrideFontsMap(Map<String, Typeface> typefaces) {
        try {
            Field e = Typeface.class.getDeclaredField("sSystemFontMap");
            e.setAccessible(true);
            Map oldFonts = (Map)e.get((Object)null);
            if(oldFonts != null) {
                oldFonts.putAll(typefaces);
            } else {
                oldFonts = typefaces;
            }

            e.set((Object)null, oldFonts);
            e.setAccessible(false);
        } catch (NoSuchFieldException var3) {
            Log.d("TypefaceUtil", "Can not set custom fonts, NoSuchFieldException");
        } catch (IllegalAccessException var4) {
            Log.d("TypefaceUtil", "Can not set custom fonts, IllegalAccessException");
        }

    }

    public static void overrideFont(String fontName, Typeface typeface) {
        try {
            Field e = Typeface.class.getDeclaredField(fontName);
            e.setAccessible(true);
            e.set((Object)null, typeface);
            e.setAccessible(false);
        } catch (Exception var3) {
            Log.d("TypefaceUtil", "Can not set custom font " + typeface.toString() + " instead of " + fontName);
        }

    }

    public static Typeface getTypeface(int fontType, Context context) {
        switch(fontType) {
            case 1:
            default:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Regular.otf");
            case 2:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Bold.otf");
            case 3:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Light_Italic.otf");
            case 4:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Bold_Italic.otf");
            case 5:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Light.otf");
            case 6:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Condensed_Regular.otf");
            case 7:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Thin.otf");
            case 8:
                return Typeface.createFromAsset(context.getAssets(), "Proxima_Nova_Semibold.otf");
        }
    }

    public static Typeface getTypefaceRegular(Context context) {
        return getTypeface(1, context);
    }

    public static Typeface getTypefaceLight(Context context) {
        return getTypeface(5, context);
    }

    public static Typeface getTypefaceBold(Context context) {
        return getTypeface(2, context);
    }

    public static Typeface getTypefaceItalic(Context context) {
        return getTypeface(3, context);
    }

    public static Typeface getTypefaceBoldItalic(Context context) {
        return getTypeface(4, context);
    }

    public static Typeface getTypefaceCondensed(Context context) {
        return getTypeface(6, context);
    }
}