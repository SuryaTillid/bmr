package cbs.com.bmr.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cbs.com.bmr.Listener.ToDoListener;
import cbs.com.bmr.R;
import cbs.com.bmr.model.ManagementRequestList;

/*********************************************************************
 * Created by Barani on 09-05-2019 in TableMateNew
 ***********************************************************************/
public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.MyHolder> {
    private ArrayList<ManagementRequestList> lists;
    private Context context;
    private LayoutInflater inflater;
    private ToDoListener listener;

    public RequestListAdapter(Context context, ArrayList<ManagementRequestList> lists) {
        this.context = context;
        this.lists = lists;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<ManagementRequestList> m_list) {
        lists = m_list;
        notifyDataSetChanged();
    }

    public void setTaskListListener(ToDoListener listener) {
        this.listener = listener;
    }

    @Override
    public RequestListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_management_request_view_row, parent, false);
        return new RequestListAdapter.MyHolder(v);
    }

    @Override
    public void onBindViewHolder(RequestListAdapter.MyHolder holder, int position) {
        ManagementRequestList taskList = lists.get(position);

        holder.t_req_dept.setText(taskList.getDepartment_name());
        holder.t_req_desc.setText(taskList.getReq_desc());
        holder.t_comments.setText(taskList.getComments());
        holder.t_req_by.setText(taskList.getReq_by());
        holder.t_req_date_time.setText(taskList.getReq_date()+"  "+taskList.getReq_time());
    }

    private String date_validate(String task_date) {
        try {
            if (!TextUtils.isEmpty(task_date)) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(task_date);
                DateFormat date_formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                task_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return task_date;
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        private TextView t_req_by,t_req_date_time,t_req_desc,t_comments,t_req_dept,t_edit,t_delete;

        public MyHolder(View v) {
            super(v);

            t_req_by = v.findViewById(R.id.t_req_name);
            t_req_date_time = v.findViewById(R.id.t_req_date_time);
            t_req_desc = v.findViewById(R.id.t_description);
            t_comments = v.findViewById(R.id.t_comments);
            t_req_dept = v.findViewById(R.id.t_department);
            t_edit = v.findViewById(R.id.t_edit);
            t_delete = v.findViewById(R.id.t_remove);

            t_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.todoList_click_listener(getAdapterPosition());
                }
            });

            t_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.requestRemoveListener(getAdapterPosition());
                }
            });
        }
    }
}