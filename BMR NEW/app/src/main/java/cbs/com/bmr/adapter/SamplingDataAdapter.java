package cbs.com.bmr.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.activity.SamplingDataList;
import cbs.com.bmr.model.PondList;
import cbs.com.bmr.model.SamplingDetails_List;

/********************************************************************
 * Created by Barani on 19-08-2019 in TableMateNew
 ********************************************************************/
public class SamplingDataAdapter extends RecyclerView.Adapter<SamplingDataAdapter.SamplingHolder>{

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<PondList> samplingDetailsList;
    private Pond_details_click_listener listener;

    public SamplingDataAdapter(Context context, ArrayList<PondList> samplingList) {
        this.context = context;
        this.samplingDetailsList = samplingList;
        inflater = LayoutInflater.from(context);
    }

    public void setSamplingListListener(Pond_details_click_listener listener) {
        this.listener = listener;
    }

    public void MyDataChanged(ArrayList<PondList> list) {
        samplingDetailsList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SamplingDataAdapter.SamplingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_sampling_data_row, parent, false);
        return new SamplingDataAdapter.SamplingHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SamplingDataAdapter.SamplingHolder holder, int position) {

        PondList list = samplingDetailsList.get(position);

        holder.t_pond_name.setText(list.getPond_id());
        holder.t_farmerName.setText(list.getFarmer_name());
        holder.t_stocking_date.setText(validateDate(list.getStocking_date()));
    }

    private String validateDate(String stocking_date) {
        try {
            if(!TextUtils.isEmpty(stocking_date))
            {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(stocking_date);
                DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy");
                stocking_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stocking_date;
    }

    @Override
    public int getItemCount() {
        return samplingDetailsList.size();
    }

    public class SamplingHolder extends RecyclerView.ViewHolder {

        TextView t_farmerName,t_pond_name,t_stocking_date;

        public SamplingHolder(@NonNull View v) {
            super(v);

            t_farmerName = v.findViewById(R.id.txt_farmer_name);
            t_pond_name = v.findViewById(R.id.txt_pond_name);
            t_stocking_date = v.findViewById(R.id.txt_stocking_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPondClick(getAdapterPosition(),samplingDetailsList.get(getAdapterPosition()).getId(),
                            samplingDetailsList.get(getAdapterPosition()).getCycle_id());
                }
            });
        }
    }
}
