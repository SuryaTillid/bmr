package cbs.com.bmr.Listener;

/*********************************************************************
 * Created by Barani on 03-05-2019 in TableMateNew
 ***********************************************************************/
public interface ToDoListener {
    void todoList_click_listener(int id);
    void requestRemoveListener(int id);
}
