package cbs.com.bmr.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;

/*********************************************************************
 * Created by Barani on 27-05-2019 in TableMateNew
 ***********************************************************************/
public class DayInActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    private Context context;
    private EditText edit_start_read, edit_close_read;
    private Button btn_day_in, btn_day_out;
    private String emp_id, date, lat_lon, display_time;
    private LinearLayout layout_check_in;
    private ConfigurationSettings settings;
    private TextView t_check_in_time;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        context = DayInActivity.this;
        settings = new ConfigurationSettings(context);
        emp_id = settings.getEmployee_ID();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        initControls();
        getLocation();
    }

    private void initControls() {
        btn_day_in = findViewById(R.id.btn_day_in);
        btn_day_out = findViewById(R.id.btn_day_out);
        t_check_in_time = findViewById(R.id.t_check_in_time);
        layout_check_in = findViewById(R.id.layout_check_in_time);
        edit_start_read = findViewById(R.id.edit_start_read);
        edit_close_read = findViewById(R.id.edit_close_read);
        btn_day_in.setOnClickListener(this);
        btn_day_out.setVisibility(View.GONE);
        edit_close_read.setVisibility(View.GONE);
        t_check_in_time.setVisibility(View.GONE);
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            String lat = String.valueOf(location.getLatitude());
            String lon = String.valueOf(location.getLongitude());
            if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lon)) {
                lat_lon = new Gson().toJson(getLocationDetails(lat, lon));
                AppLog.write("LAT_LON", lat_lon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onClick(View v) {
        getLocation();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat display_date_format = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        date = date_format.format(calendar.getTime());
        display_time = display_date_format.format(calendar.getTime());
        if (!TextUtils.isEmpty(lat_lon) && !TextUtils.isEmpty(date)) {
            String start_meter = edit_start_read.getText().toString().trim();
            new Day_In_submit().execute(emp_id, date, lat_lon, start_meter);
        } else {
            Toast.makeText(context, "Please Press again..!", Toast.LENGTH_SHORT).show();
            getLocation();
        }
    }

    private class Day_In_submit extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.EmployeeIn_Create_attendance((App) getApplication(), p[0], p[1], p[2], p[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    //Toast.makeText(context, "Check-In Successfully..!", Toast.LENGTH_SHORT).show();
                    settings.setIS_CHECKED_IN(true);
                    settings.setCHECKED_IN_TIME(display_time);
                    settings.setUPDATE_ID(success.getUpdate_id());
                    t_check_in_time.setText("CheckIn at :  " + settings.getCHECKED_IN_TIME());
                    moveToMainActivity();
                } else {
                    Toast.makeText(context, "Failed to Check-In..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to Check-In..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void moveToMainActivity() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Your Attendance has been recorded successfully");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, Home_Page_Activity.class);
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}