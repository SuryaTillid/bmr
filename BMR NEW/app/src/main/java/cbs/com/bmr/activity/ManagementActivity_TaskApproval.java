package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.TaskApprovalListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TaskList;
import cbs.com.bmr.model.ZoneMaster;

/*********************************************************************
 * Created by Barani on 26-06-2019 in TableMateNew
 ***********************************************************************/
public class ManagementActivity_TaskApproval extends Fragment implements On_BackPressed, TaskDetails_ClickListener {

    private Context context;
    private RecyclerView recyclerView_tasksList;
    private EditText edit_zone;
    private LinearLayoutManager mLayoutManager;
    private ConfigurationSettings settings;
    private TaskApprovalListAdapter adapter;
    private String approval_status;
    private String zoneID,zoneNAME;
    private ArrayList<TaskList> taskList = new ArrayList<>();
    private ArrayList<ZoneMaster> zoneMasterList = new ArrayList<>();
    private ArrayList<String> zone_list_spinner = new ArrayList<>();
    private TextView from_date_to_display, to_date_display;
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener from_date_select, to_date_select;
    private String startDate, endDate;
    private Button btn_search_by_date;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_task_approval, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);
        zoneID = settings.getREGION_ID();

        recyclerView_tasksList = view.findViewById(R.id.rc_taskDetails_list);
        edit_zone = view.findViewById(R.id.edit_region_name);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        from_date_to_display = view.findViewById(R.id.from_date_to_display);
        to_date_display = view.findViewById(R.id.to_date_to_display);
        btn_search_by_date = view.findViewById(R.id.btn_search_by_date);

        setDate();

        /*
         * When refresh items using P to R
         * */
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refresh();
                stop();
            }
        });

        adapter = new TaskApprovalListAdapter(context,taskList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setTaskListListener(this);
        recyclerView_tasksList.setLayoutManager(mLayoutManager);
        recyclerView_tasksList.setAdapter(adapter);
        AppLog.write("T1", "--" + new Gson().toJson(taskList));

        new RegionList().execute();

        btn_search_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = from_date_to_display.getText().toString().trim();
                endDate = to_date_display.getText().toString().trim();
                AppLog.write("FROM_TO_Date", "--" + startDate + "--" + endDate);
                new GetTasksList().execute(startDate, endDate);
            }
        });

        from_date_to_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, from_date_select, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        to_date_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, to_date_select, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        to_date_select = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                getEndDate();
            }
        };

        from_date_select = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                getStartDate();
            }
        };

        edit_zone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSpinnerForZoneSelection();
            }
        });

        return view;
    }

    private void refresh() {
        new GetTasksList().execute(startDate, endDate);
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void getEndDate() {
        SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
        to_date_display.setText(date_format.format(myCalendar.getTime()));
        endDate = to_date_display.getText().toString().trim();
    }

    private void getStartDate() {
        SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
        from_date_to_display.setText(date_format.format(myCalendar.getTime()));
        startDate = from_date_to_display.getText().toString().trim();
    }

    private void setDate() {
        SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
        from_date_to_display.setText(date_format.format(myCalendar.getTime()));
        to_date_display.setText(date_format.format(myCalendar.getTime()));
        startDate = from_date_to_display.getText().toString().trim();
        endDate = to_date_display.getText().toString().trim();
        //call Tasks List
        new GetTasksList().execute(startDate, endDate);
    }

    @Override
    public boolean onBackPressed()
    {
        TaskListFragment fragment = new TaskListFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
        return false;
    }

    @Override
    public void onTaskClick(int position, String id, String check) {

    }

    @Override
    public void feedbackSubmit(String id, String task_type) {

    }

    @Override
    public void onClickEdit(int position, String t_id, String task_id) {

    }

    @Override
    public void onClickApproval(int position, final String t_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Task Approval Status");
        builder.setPositiveButton("Approve", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "1";
                new TaskApprovalCheck().execute(t_id, "1");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "2";
                new TaskApprovalCheck().execute(t_id, "2");
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void callSpinnerForZoneSelection() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_customer_list_spinner);

        final Spinner spinner_customer_list = dialog.findViewById(R.id.customer_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        TextView t_spinner_header = dialog.findViewById(R.id.t_spinner_header);
        t_spinner_header.setText("Select Region");
        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, zone_list_spinner);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_customer_list.setAdapter(ad);
        spinner_customer_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                zoneID = zoneMasterList.get(position).getId();
                zoneNAME = zoneMasterList.get(position).getZone_name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(zoneID)) {
                    populateRegionDetails(zoneID);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void populateRegionDetails(String zoneID) {
        for (ZoneMaster c : zoneMasterList) {
            if (zoneID.equalsIgnoreCase(c.getId())) {
                edit_zone.setText(c.getZone_name());
                new GetTasksList().execute(startDate,endDate);
            }
        }
    }


    private class GetTasksList extends AsyncTask<String, String, ArrayList<TaskList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TaskList> doInBackground(String... v) {
            ArrayList<TaskList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getTaskDetailsList((App) getActivity().getApplication(), settings.getEmployee_ID(),zoneID ,v[0],v[1]);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<TaskList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                taskList.clear();
                for (TaskList t : list)
                {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(t.getTask_date()));
                    taskList.add(t);
                }
                adapter.MyDataChanged(taskList);
            } else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class TaskApprovalCheck extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckApprovedStatus((App) getActivity().getApplication(), settings.getEmployee_ID(), p[0], p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (approval_status.equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Approved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Cancelled", Toast.LENGTH_SHORT).show();
                }
                refresh();
            }
        }
    }

    private class RegionList extends AsyncTask<Void, Void, ArrayList<ZoneMaster>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<ZoneMaster> doInBackground(Void... v) {
            ArrayList<ZoneMaster> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getZone_List((App) getActivity().getApplication());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<ZoneMaster> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            zone_list_spinner.clear();
            for (ZoneMaster z_list : list) {
                zoneMasterList.add(z_list);
                zone_list_spinner.add(z_list.getZone_name());
            }
        }
    }
}
