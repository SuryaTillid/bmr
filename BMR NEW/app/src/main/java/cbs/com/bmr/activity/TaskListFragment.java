package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.TaskListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TaskList;

/*********************************************************************
 * Created by Barani on 22-03-2019 in TableMateNew
 ***********************************************************************/
public class TaskListFragment extends Fragment implements TaskDetails_ClickListener, LocationListener, On_BackPressed {
    private TaskListAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recyclerView_task;
    private ArrayList<TaskList> taskList = new ArrayList<>();
    private Context context;
    private String check_in_out, task_id, lat_lon, check_date;
    private String approval_status;
    LocationManager locationManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ConfigurationSettings settings;
    private TextView from_date_to_display, to_date_display;
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener from_date_select, to_date_select;
    private String startDate, endDate;
    private Button btn_search_by_date;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_task_main, container, false);

        context = getActivity();
        settings = new ConfigurationSettings(context);

        getActivity().setTitle("Task List");

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        recyclerView_task = view.findViewById(R.id.rc_TaskDetailsList);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        from_date_to_display = view.findViewById(R.id.from_date_to_display);
        to_date_display = view.findViewById(R.id.to_date_to_display);
        btn_search_by_date = view.findViewById(R.id.btn_search_by_date);

        adapter = new TaskListAdapter(context, taskList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setTaskListListener(this);
        recyclerView_task.setLayoutManager(mLayoutManager);
        recyclerView_task.setAdapter(adapter);

        setDate();
        getLocation();

        /*
         * When refresh items using P to R
         * */
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refresh();
                stop();
            }
        });

        btn_search_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = from_date_to_display.getText().toString().trim();
                endDate = to_date_display.getText().toString().trim();
                AppLog.write("FROM_TO_Date", "--" + startDate + "--" + endDate);
                new GetTaskList().execute(startDate, endDate);
            }
        });

        from_date_to_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, from_date_select, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        to_date_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, to_date_select, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        to_date_select = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                getEndDate();
            }
        };

        from_date_select = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                getStartDate();
            }
        };

        return view;
    }

    private void getEndDate() {
        SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
        to_date_display.setText(date_format.format(myCalendar.getTime()));
        endDate = to_date_display.getText().toString().trim();
    }

    private void getStartDate() {
        SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
        from_date_to_display.setText(date_format.format(myCalendar.getTime()));
        startDate = from_date_to_display.getText().toString().trim();
    }

    private void setDate() {
        SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
        from_date_to_display.setText(date_format.format(myCalendar.getTime()));
        to_date_display.setText(date_format.format(myCalendar.getTime()));
        startDate = from_date_to_display.getText().toString().trim();
        endDate = to_date_display.getText().toString().trim();
        //call Tasks List
        new GetTaskList().execute(startDate, endDate);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Home_Page_Activity) getActivity()).setActionBarTitle("Task List");
    }

    private void refresh() {
        new GetTaskList().execute(startDate, endDate);
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /*
     * On Back Pressed - Call
     * */
    @Override
    public boolean onBackPressed() {
       // getConfirmation();
        callHomeActivity();
        return true;
    }

    private void callHomeActivity() {
        Intent myIntent = new Intent(getActivity(), Home_Page_Activity.class);
        getActivity().startActivity(myIntent);
    }

    @Override
    public void onTaskClick(int position, String id, String check) {
        AppLog.write("TAG_GEO", lat_lon);
        check_in_out = check;
        task_id = id;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        check_date = date_format.format(calendar.getTime());
        AppLog.write("TAG_GEO", "-" + task_id + "--" + check_in_out + "-" + check_date + "--" + lat_lon);
        if (!TextUtils.isEmpty(lat_lon) && !TextUtils.isEmpty(check_date)) {
            if (check_in_out.equalsIgnoreCase("1")) {
                new GetCheck_In_Details().execute(task_id, check_date, lat_lon);
            } else {
                new GetCheck_Out_Details().execute(task_id, check_date, lat_lon);
            }
        } else {
            Toast.makeText(context, "Location not detected, please try again..!", Toast.LENGTH_SHORT).show();
            getLocation();
        }
    }

    @Override
    public void feedbackSubmit(String id, String type) {
        Bundle bundle = new Bundle();
        bundle.putString("ID", id);
        bundle.putString("TYPE", type);
        FeedbackFromCustomer fragment = new FeedbackFromCustomer();
        fragment.setArguments(bundle);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClickEdit(int position, String id, String task_id) {
        Bundle bundle = new Bundle();
        bundle.putString("T_ID", id);
        bundle.putString("TASK_ID", task_id);
        bundle.putString("S_DATE", startDate);
        bundle.putString("E_DATE", endDate);
        TaskUpdateFragment fragment = new TaskUpdateFragment();
        fragment.setArguments(bundle);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClickApproval(int position, final String t_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Task Approval Status");
        builder.setPositiveButton("Approve", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "1";
                new TaskApprovalCheck().execute(t_id, "1");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "2";
                new TaskApprovalCheck().execute(t_id, "2");
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            //LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, this);
            /*Location location= locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            String lat = String.valueOf(location.getLatitude());
            String lon = String.valueOf(location.getLongitude());
            AppLog.write("Called-","----1"+lat+"--"+lon);*/
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String lat = String.valueOf(location.getLatitude());
            String lon = String.valueOf(location.getLongitude());
            String address = addresses.get(0).getAddressLine(0);
            if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lon)) {
                lat_lon = new Gson().toJson(getLocationDetails(lat, lon));
                AppLog.write("LAT_LON", lat_lon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callCheck_in_out(String lat_lon, String check_date, String check_in_out, String task_id) {
        if (check_in_out.equalsIgnoreCase("1")) {
            new GetCheck_In_Details().execute(task_id, check_date, lat_lon);
        } else {
            new GetCheck_Out_Details().execute(task_id, check_date, lat_lon);
        }
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(context, "Please Enable GPS and Internet..!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    private class GetTaskList extends AsyncTask<String, String, ArrayList<TaskList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TaskList> doInBackground(String... v) {
            final String url = App.IP_ADDRESS + "/index.php/tasksummary/getlisttaskapi";
            AppLog.write("URL:::", url);
            ArrayList<TaskList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getTaskDetailsList((App) getActivity().getApplication(), settings.getEmployee_ID(), settings.getREGION_ID(), v[0], v[1]);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<TaskList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                taskList.clear();
                for (TaskList t : list) {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(t.getTask_date()));
                    taskList.add(t);
                }
                adapter.MyDataChanged(taskList);
            } else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetCheck_In_Details extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckIn_Task_update((App) getActivity().getApplication(), p[0], p[1], p[2],settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("CHECK_IN", "--" + new Gson().toJson(success));
            if (success != null)
            {
                try {
                    if (success.getCheckin_status().equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), "Check-in Successfully..!", Toast.LENGTH_SHORT).show();
                        refresh();
                    }else if(success.getCheckin_status().equalsIgnoreCase("2"))
                    {
                        Toast.makeText(getActivity(), "You can't do check-in for this user..!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getActivity(), "Failed to Check-in..!", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-in..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetCheck_Out_Details extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckOut_Task_update((App) getActivity().getApplication(), p[0], p[1], p[2],settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("CHECK_OUT", "---" + new Gson().toJson(success));
            if (success != null) {
                if (success.getCheckout_status().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Check-out Successfully..!", Toast.LENGTH_SHORT).show();
                    refresh();
                }
                else if(success.getCheckin_status().equalsIgnoreCase("2"))
                {
                    Toast.makeText(getActivity(), "You can't do check-out for this user..!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_sign_out) {
            getConfirmation();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void getConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit, your session will be cleared?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(context, LoginActivity.class);
                startActivity(i);
                settings.clearSession();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class TaskApprovalCheck extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckApprovedStatus((App) getActivity().getApplication(), settings.getEmployee_ID(), p[0], p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (approval_status.equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Approved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Cancelled", Toast.LENGTH_SHORT).show();
                }
                refresh();
            }
        }
    }
}

    /*private class TaskDetailsComparator implements Comparator<TaskList> {
        @Override
        public int compare(TaskList v1, TaskList v2) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            public int compare(String lhs, String rhs)
            {
                return dateFormat.parse(lhs).compareTo(dateFormat.parse(rhs));
            }
            return 0;
        }
    }*/