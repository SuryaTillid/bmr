package cbs.com.bmr.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.PrimarySettings;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;

/*********************************************************************
 * Created by Barani on 26-03-2019 in TableMateNew
 ***********************************************************************/
public class ConfigurationActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText ed_IpAddress;
    private String IpAddress;
    private Context context;
    private ConfigurationSettings configurationSettings;
    private PrimarySettings primarySettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        context = ConfigurationActivity.this;
        configurationSettings = new ConfigurationSettings(context);
        primarySettings = new PrimarySettings(context);

        IpAddress = primarySettings.getIP_ADDRESS();

        Button btn_Submit = findViewById(R.id.btn_Submit);
        Button btn_Cancel = findViewById(R.id.btn_Cancel);
        ed_IpAddress = findViewById(R.id.ed_IpAddress);

        btn_Cancel.setOnClickListener(this);
        btn_Submit.setOnClickListener(this);

        if(primarySettings.isConfigured()) {
            AppLog.write("Ip_Address","----"+primarySettings.getIP_ADDRESS());
            ed_IpAddress.setText(IpAddress);
        }
        else {
            AppLog.write("Empty","----");
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id== R.id.btn_Cancel) {
            exit_app();
        }
        if(id== R.id.btn_Submit)
        {
            if (ed_IpAddress.getText().toString().equals("")) {
                ed_IpAddress.setError("Field cannot be left blank..!");
            } else {
                IpAddress = ed_IpAddress.getText().toString().trim();
                IpAddress = IpAddress.replaceAll("\\s", "");
                configurationSettings.setIP_ADDRESS(IpAddress);
                primarySettings.setIP_ADDRESS(IpAddress);
                AppLog.write("IP_Address--", "---" + IpAddress);
                App.IP_ADDRESS = IpAddress;
                if(!TextUtils.isEmpty(primarySettings.getIP_ADDRESS()))
                {
                    configurationSettings.setIS_CONFIGURED();
                    primarySettings.setIS_CONFIGURED();
                }
                callMainActivity();
            }
        }
    }

    private void callMainActivity() {
        Intent in = new Intent(context, LoginActivity.class);
        startActivity(in);
    }

    private void exit_app()
    {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    @Override
    public void onBackPressed()
    {
        confirmExit();
    }

    private void confirmExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        }); builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                exit_app();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}