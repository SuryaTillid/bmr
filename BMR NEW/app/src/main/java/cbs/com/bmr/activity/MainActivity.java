package cbs.com.bmr.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;

import com.google.android.gms.location.LocationListener;

/*********************************************************************
 * Created by Barani on 22-03-2019 in TableMateNew
 ***********************************************************************/

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private Toolbar mToolbar;
    private static final int REQUEST_ENABLE_LOCATION = 4;
    private FragmentDrawer drawerFragment;
    private Context context;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private ConfigurationSettings settings;
    private String userName;
    public Double lLat, lLng;
    private String Cur_latitude, Cur_longitude;
    Location mLastLocation, location;
    private static final long INTERVAL = 1000 * 600;
    private static final long FASTEST_INTERVAL = 1000 * 300;
    private long t_stamp = System.currentTimeMillis();
    private Calendar myCalendar = Calendar.getInstance();
    Window window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;
        settings = new ConfigurationSettings(context);
        mToolbar = findViewById(R.id.toolbar);

        initMethods();
        userName = settings.getUSER_NAME();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        displayView(2);
    }

    private void initMethods() {
        initLocationRequest();
        buildGoogleApiClient();
        displayLocationSettingsRequest();
    }

    private void displayLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder =
                new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest).setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MainActivity.this, REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @SuppressWarnings("deprecation")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        lLat = location.getLatitude();
        lLng = location.getLongitude();
        getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 4) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                }
            }
        }
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Cur_latitude = String.valueOf(latitude);
            Cur_longitude = String.valueOf(longitude);
            String lat_lon = new Gson().toJson(getLocationDetails(Cur_latitude, Cur_longitude));
            AppLog.write("LL------", lat_lon);
            String time_value = getTime();
            new SendUpdatedLocation().execute(time_value,lat_lon);
        }
    }

    private String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String s = sdf.format(timestamp);
        return s;
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        if (id == R.id.action_sign_out) {
            exitFromApp();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void exitFromApp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to Logout, your session will be cleared?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(context, LoginActivity.class);
                startActivity(i);
                settings.clearSession();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new AttendanceFragment();
                title = getString(R.string.title_attendance);
                break;
            case 1:
                fragment = new ChangePasswordFragment();
                title = getString(R.string.title_change_password);
                break;
       /* case 2:
            fragment = new ManagementRequest();
            title = getString(R.string.title_mgmt_request);
            break;
        case 3:
            fragment = new ManagementActivity_TaskApproval();
            title = getString(R.string.title_task_approval);
            break;*/
            case 2:
                CurrentDateTaskFragment fragment_c = new CurrentDateTaskFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.container_body, fragment_c).commit();
                title = getString(R.string.title_today_task);
                getSupportActionBar().setTitle(title);
                break;
            case 3:
                fragment = new TaskListFragment();
                title = getString(R.string.title_home);
                break;
            case 4:
                fragment = new TaskSchedulerFragment();
                title = getString(R.string.title_task_scheduler);
                break;
        /*case 7:
            fragment = new ToDoList_Fragment();
            title = getString(R.string.title_todo_list);
            break;*/
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }


    @Override
    public void onBackPressed() {
        callFragments();
    }

    private void callFragments() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment f : fragments) {
            if (f instanceof TaskSchedulerFragment)
                ((TaskSchedulerFragment) f).onBackPressed();
            if (f instanceof TaskUpdateFragment)
                ((TaskUpdateFragment) f).onBackPressed();
            if (f instanceof AttendanceFragment)
                ((AttendanceFragment) f).onBackPressed();
            if (f instanceof CurrentDateTaskFragment)
                ((CurrentDateTaskFragment) f).onBackPressed();
            if (f instanceof CreateTaskForTodayFragment)
                ((CreateTaskForTodayFragment) f).onBackPressed();
            if (f instanceof TaskListFragment)
                ((TaskListFragment) f).onBackPressed();
            if (f instanceof FeedbackFromCustomer)
                ((FeedbackFromCustomer) f).onBackPressed();
            if (f instanceof TaskUpdateFragmentForCurrentDayFragment)
                ((TaskUpdateFragmentForCurrentDayFragment) f).onBackPressed();
            if (f instanceof ToDoList_Fragment)
                ((ToDoList_Fragment) f).onBackPressed();
            if (f instanceof ManagementRequest)
                ((ManagementRequest) f).onBackPressed();
            if (f instanceof ChangePasswordFragment)
                ((ChangePasswordFragment) f).onBackPressed();
            if (f instanceof ManagementActivity_TaskCreate)
                ((ManagementActivity_TaskCreate) f).onBackPressed();
            if (f instanceof ManagementCreateTaskForTodayFragment)
                ((ManagementCreateTaskForTodayFragment) f).onBackPressed();
            if (f instanceof ManagementActivity_TaskApproval)
                ((ManagementActivity_TaskApproval) f).onBackPressed();
        }
    }

    private class SendUpdatedLocation extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.postEmployeeGPSLog((App) getApplication(), settings.getEmployee_ID(), p[0],p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage successMessage) {
            super.onPostExecute(successMessage);
            dialog.dismiss();
            AppLog.write("Response", "---" + new Gson().toJson(successMessage));
        }
    }
}