package cbs.com.bmr.configuration;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class AppLog {
    public static final boolean isDebugMode = true;
    public static final boolean isDebugUrl = true;

    /**
     * Method to write logs on debug mode
     *
     * @param TAG
     * @param message
     */
    public static void write(String TAG, String message) {
        if (isDebugMode)
            Log.i("" + TAG, "" + message);
    }

    public static void info(String TAG, String message) {
        if (isDebugMode)
            Log.i("" + TAG, "" + message);
    }

    /**
     * Method to show Toast on debug mode
     *
     * @param p_context
     * @param p_message
     */
    public static void debugToast(Context p_context, String p_message) {
        if (isDebugMode)
            Toast.makeText(p_context, "" + p_message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method to show toast
     *
     * @param p_context
     * @param p_message
     */
    public static void showToast(Context p_context, String p_message) {
        Toast.makeText(p_context, "" + p_message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method to show alert dialog
     *
     * @param p_context
     * @param p_title
     * @param p_message
     */


    public static void showDialog(Context p_context, @Nullable String p_title, String p_message) {
        if (null != p_context && !TextUtils.isEmpty(p_message)) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(p_context);
            if (!TextUtils.isEmpty(p_title))
                alert.setTitle(p_title);
            alert.setMessage(p_message);
            alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alert.create().dismiss();
                }
            });
            alert.create().show();
        }
    }
}