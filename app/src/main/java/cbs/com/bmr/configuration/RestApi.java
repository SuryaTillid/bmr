package cbs.com.bmr.configuration;

import java.util.ArrayList;

import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.EmployeeInfoForManagementRequest;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.FeedbackQuestions;
import cbs.com.bmr.model.ManagementRequestList;
import cbs.com.bmr.model.PondDetailedList;
import cbs.com.bmr.model.PondList;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TODO_Taskslist;
import cbs.com.bmr.model.TaskList;
import cbs.com.bmr.model.ZoneMaster;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public interface RestApi {

    @FormUrlEncoded
    @POST("index.php/tasksummary/createtaskapi")
    Call<SuccessMessage> createTask(@Field("task_date") String task_date, @Field("task_type") String task_type,
                                    @Field("customer_id") String customer_id, @Field("description") String description);

    @FormUrlEncoded
    @POST("index.php/tasksummary/updatetasksummaryapi")
    Call<SuccessMessage> updateTaskSummary(@Field("task_id") String task_id, @Field("td_id") String td_id,
                                           @Field("task_date") String task_date,
                                           @Field("created_by_id") String created_by_id,
                                           @Field("approved_by") String approved_by_id,
                                           @Field("created_date") String created_date, @Field("taskSchedule") String task);

   /* @GET("index.php/tasksummary/getlisttaskapi/{emp_id}")
    Call<ArrayList<TaskList>> getTaskList(@Path("emp_id") String emp_id);*/

    @FormUrlEncoded
    @POST("index.php/tasksummary/getlisttaskapi")
    Call<ArrayList<TaskList>> getTaskList(@Field("emp_id") String emp_id, @Field("region_id") String region_id,
                                          @Field("from_date") String from_date, @Field("to_date") String to_date);

    @FormUrlEncoded
    @POST("index.php/customers/getlistapi")
    Call<ArrayList<CustomerList>> getCustomerList(@Field("login_id") String emp_id);

    @GET("index.php/regionalzonemaster/getlistapi")
    Call<ArrayList<ZoneMaster>> getZoneList();

    @GET("index.php/employee/getlistapi")
    Call<ArrayList<EmployeeList>> getEmployee_List();

    @GET("index.php/employee/getdepartmentlistapi")
    Call<ArrayList<EmployeeInfoForManagementRequest>> getDepartment_List();

    @GET("index.php/feedbackquestion/getlist")
    Call<ArrayList<FeedbackQuestions>> getFeedbackQuestions(@Query("type") String type);

    @FormUrlEncoded
    @POST("/index.php/tasksummary/createtasksummaryapi")
    Call<SuccessMessage> createTaskSummary(@Field("task_id") String task_id, @Field("task_date") String task_date,
                                           @Field("created_by_id") String created_by_id,
                                           @Field("approved_by") String approved_by_id,
                                           @Field("assigned_by_id") String assigned_by_id,
                                           @Field("created_date") String created_date, @Field("taskSchedule") String taskSchedule);

    @FormUrlEncoded
    @POST("/index.php/feedbackQuestion/feedbackquestionsubmitapi")
    Call<SuccessMessage> feedbackSubmit(@Field("td_id") String td_id, @Field("fb_submit_time") String fb_submit,
                                        @Field("fb_geo_json") String fb_geo,
                                        @Field("feedBack") String feedback,
                                        @Field("login_id") String emp_id, @Field("fb_comments") String comments);

    @FormUrlEncoded
    @POST("/index.php/employee/loginapi")
    Call<SuccessMessage> login_bmr(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("/index.php/employee/createattendance")
    Call<SuccessMessage> employee_in_create_attendance(@Field("empid") String emp_id, @Field("check_in") String check_in,
                                                       @Field("geo_checkin") String geo_in, @Field("starting_meter") String starting_meter);

    @FormUrlEncoded
    @POST("/index.php/employee/updateattendance")
    Call<SuccessMessage> employee_out_update_attendance(@Field("empid") String emp_id, @Field("update_id") String update_id,
                                                        @Field("check_out") String check_out,
                                                        @Field("geo_checkout") String geo_checkout, @Field("closing_meter") String closing_meter);

    @FormUrlEncoded
    @POST("index.php/tasksummary/tasksummarycheckinapi")
    Call<SuccessMessage> check_in_task(@Field("td_id") String id, @Field("check_in") String check_in,
                                       @Field("geo_checkin") String geo_in, @Field("login_id") String emp_id);

    @FormUrlEncoded
    @POST("index.php/tasksummary/tasksummarycheckoutapi")
    Call<SuccessMessage> check_out_task(@Field("td_id") String id, @Field("check_out") String check_in,
                                        @Field("geo_checkout") String geo_out, @Field("login_id") String emp_id);

    @GET("index.php/taskSummary/gettodolistapi")
    Call<ArrayList<TODO_Taskslist>> getTodo_Tasks_list();

    @FormUrlEncoded
    @POST("index.php/taskSummary/gettaskdataapi")
    Call<ArrayList<TODO_Taskslist>> get_Task_By_ID(@Field("task_id") String task_id);

    /*Create To-do List*/
    @FormUrlEncoded
    @POST("index.php/taskSummary/createtodotaskapi")
    Call<SuccessMessage> create_Task_TodoList(@Field("task_text") String task_text, @Field("task_due") String task_due,
                                              @Field("task_notes") String task_notes, @Field("created_by") String created_by);

    /*Update To-do List*/
    @FormUrlEncoded
    @POST("index.php/taskSummary/updatetodotaskapi")
    Call<SuccessMessage> update_Task_TodoList(@Field("task_id") String task_id, @Field("task_text") String task_text, @Field("task_due") String task_due,
                                              @Field("task_notes") String task_notes);

    /*Delete Task*/
    @FormUrlEncoded
    @POST("index.php/taskSummary/deletetaskapi")
    Call<SuccessMessage> delete_task(@Field("task_id") String task_id);

    /*Task Approval - status update*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/updatetaskstatusapi")
    Call<SuccessMessage> check_approved_status(@Field("user_id") String user_id, @Field("taskid") String task_id,
                                               @Field("approvedstatus") String approved_status);

    /*Get List of Management Requests*/
    @GET("index.php/managementrequest/getlist/{id}")
    Call<ArrayList<ManagementRequestList>> getRequestsList(@Path("id") String id);

    /*Create Management Request*/
    @FormUrlEncoded
    @POST("index.php/managementRequest/createRequest")
    Call<SuccessMessage> createManagementRequest(@Field("req_by") String emp_id, @Field("req_dep") String dept_id,
                                                 @Field("req_desc") String req_desc, @Field("comments") String comments,
                                                 @Field("req_qty") String req_qty, @Field("expected_budget") String expect_budget);

    /*Update - Management Request*/
    @FormUrlEncoded
    @POST("index.php/managementRequest/updateRequest")
    Call<SuccessMessage> updateManagementRequest(@Field("id") String c_id, @Field("req_by") String emp_id, @Field("req_dep") String dept_id,
                                                 @Field("req_desc") String req_desc, @Field("comments") String comments,
                                                 @Field("req_qty") String req_qty, @Field("expected_budget") String expect_budget);

    /*Delete Management Request*/
    @FormUrlEncoded
    @POST("index.php/managementRequest/deleteRequest")
    Call<SuccessMessage> deleteManagementRequest(@Field("req_id") String request_id);

    /*Retrieve Employee Information&Department Details*/
    @GET("index.php/managementRequest/getemployeedepartment/{id}")
    Call<EmployeeInfoForManagementRequest> getEmployeeInfoList(@Path("id") String id);

    /*Day in status verification*/
    @GET("index.php/employee/dayinverification")
    Call<SuccessMessage> getDayInStatus(@Query("emp_id") String emp_id);

    /*change Password*/
    @FormUrlEncoded
    @POST("index.php/employee/changepasswordapi")
    Call<SuccessMessage> changePassword(@Field("emp_id") String emp_id, @Field("password") String password);

    /*
     * Send Mail
     * */
    @FormUrlEncoded
    @POST("index.php/employee/sendcustomermail")
    Call<SuccessMessage> sendMail(@Field("guest_email") String guest_email, @Field("emp_name") String emp_name,
                                  @Field("task_id") String customer_name, @Field("comments") String comments);

    /*
     * Create Customer
     * */
    @FormUrlEncoded
    @POST("index.php/customers/customercreateapi")
    Call<SuccessMessage> createCustomerInfo(@Field("contact_no") String c_num, @Field("customercategory") String category,
                                            @Field("firstname") String fName,
                                            @Field("lastname") String lName,
                                            @Field("address1") String address1, @Field("address2") String address2,
                                            @Field("city") String city, @Field("state") String state,
                                            @Field("zone") String zone, @Field("comments") String comments,
                                            @Field("cust_type") String cust_type, @Field("customerof") String customerof,
                                            @Field("status") String status);

    /*
     * Update Customer
     * */
    @FormUrlEncoded
    @POST("index.php/customers/customerupdateapi")
    Call<SuccessMessage> updateCustomerInfo(@Field("custid") String id, @Field("contact_no") String c_num,
                                            @Field("customercategory") String category,
                                            @Field("firstname") String fName, @Field("lastname") String lName,
                                            @Field("address1") String address1, @Field("address2") String address2,
                                            @Field("city") String city, @Field("state") String state,
                                            @Field("zone") String zone, @Field("comments") String comments,
                                            @Field("cust_type") String cust_type, @Field("customerof") String customerof,
                                            @Field("status") String status);

    /*
     * POND Creation
     * */
    @FormUrlEncoded
    @POST("index.php/tasksummary/createpondapi")
    Call<SuccessMessage> createPond(@Field("emp_id") String emp_id, @Field("cust_id") String cust_id, @Field("pond_id") String pond_id,
                                    @Field("comments") String comments, @Field("size") String size, @Field("density") String density,
                                    @Field("pond_refer") String pond_ref);

    /*Employee GPS*/
    @FormUrlEncoded
    @POST("index.php/employee/employeegpslog")
    Call<SuccessMessage> employee_GPS(@Field("emp_id") String emp_id, @Field("timestamp") String timestamp,
                                      @Field("gps_coordinates") String gps_coordinates);

    /*
     * POND Creation
     * */
    @FormUrlEncoded
    @POST("index.php/tasksummary/createpondapi")
    Call<SuccessMessage> createPond(@Field("emp_id") String emp_id, @Field("cust_id") String cust_id, @Field("pond_id") String pond_id,
                                    @Field("comments") String comments, @Field("size") String size, @Field("density") String density,
                                    @Field("pond_refer") String pond_ref, @Field("status") String status, @Field("wsa") String wsa,
                                    @Field("salinity") String salinity, @Field("seed_stocking") String seed_stocking, @Field("ph") String ph, @Field("stocking_date") String stocking_date,
                                    @Field("recorded_date") String recorded_date);

    /*
     * POND Update
     * */
    @FormUrlEncoded
    @POST("index.php/tasksummary/updatepondapi")
    Call<SuccessMessage> updatePond(@Field("emp_id") String emp_id, @Field("cust_id") String cust_id, @Field("pondid") String pond_id,
                                    @Field("cycle_id") String cycle_id, @Field("pond_id") String cust_pond_id,
                                    @Field("comments") String comments, @Field("size") String size, @Field("density") String density,
                                    @Field("pond_refer") String pond_ref, @Field("status") String status, @Field("wsa") String wsa,
                                    @Field("seed_stocking") String seed_stocking, @Field("ph") String ph, @Field("salinity") String salinity,
                                    @Field("stocking_date") String stocking_date,
                                    @Field("recorded_date") String recorded_date);

    /*Get POND List*/
    @GET("index.php/tasksummary/getpondlist")
    Call<ArrayList<PondList>> getPondList(@Query("cust_id") String customer_id);

    /*Get POND Details List*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/getponddata")
    Call<PondDetailedList> getPondDetailedList(@Field("cycle_id") String cycle_id, @Field("pondid") String pond_id);

    /*Create Sampling*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/createsampling")
    Call<SuccessMessage> createSampling(@Field("cycle_id") String cycle_id, @Field("emp_id") String emp_id,
                                        @Field("daily_feed") String daily_feed, @Field("recorded_date") String recorded_date,
                                        @Field("sample_harvest_flag") String sample_harvest_flag,
                                        @Field("abw") String abw);

    /*Update Sampling*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/updatesampling")
    Call<SuccessMessage> updateSampling(@Field("sample_id ") String sample_id, @Field("emp_id") String emp_id,
                                        @Field("daily_feed") String daily_feed, @Field("recorded_date") String recorded_date,
                                        @Field("sample_harvest_flag") String sample_harvest_flag,
                                        @Field("abw") String abw);

    /*Get Cycling detailed list*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/getPondlistSampling")
    Call<ArrayList<PondList>> getPondSamplingList(@Field("cust_id") String customer_id);
}