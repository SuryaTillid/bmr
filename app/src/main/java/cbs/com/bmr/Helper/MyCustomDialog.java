package cbs.com.bmr.Helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import cbs.com.bmr.R;

/*********************************************************************
 * Created by Barani on 26-03-2019 in TableMateNew
 ***********************************************************************/
public class MyCustomDialog extends ProgressDialog {
    String textMessage;
    private TextView Text_Message;

    public MyCustomDialog(Context context, String p_message) {
        super(context);
        textMessage = p_message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        Text_Message = findViewById(R.id.Text_Message);

        if (!TextUtils.isEmpty(textMessage)) {
            Text_Message.setText(textMessage);
            Text_Message.setTextColor(Color.parseColor("#182e4e"));
            Text_Message.setTextSize(16);
        }
    }
}
