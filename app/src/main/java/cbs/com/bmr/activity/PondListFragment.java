package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.PondNameListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.PondList;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public class PondListFragment extends Fragment implements On_BackPressed, Pond_details_click_listener {

    private PondNameListAdapter adapter;
    private EditText edit_customer;
    private String customerID, customerNAME;
    private Context context;
    private ConfigurationSettings settings;
    private RecyclerView rc_pond_list;
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<PondList> pondList = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private FloatingActionButton fab_pond_create;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pond_list, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);
        initialize(view);

        new GetCustomerList().execute();
        new GetPondList().execute(customerID);

        edit_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSpinnerForCustomerSelection();
            }
        });

        adapter = new PondNameListAdapter(context, pondList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setPondListListener(this);
        rc_pond_list.setLayoutManager(mLayoutManager);
        rc_pond_list.setAdapter(adapter);

        fab_pond_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PondCreationFragment fragment = new PondCreationFragment();
                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Home_Page_Activity) getActivity()).setActionBarTitle("Pond List");
    }

    private void callSpinnerForCustomerSelection() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_customer_list_spinner);

        final Spinner spinner_customer_list = dialog.findViewById(R.id.customer_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, customer_list_spinner);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_customer_list.setAdapter(ad);
        spinner_customer_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerID = customerList.get(position).getId();
                customerNAME = customerList.get(position).getFirst_name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(customerID)) {
                    populateCustomerDetails(customerID);
                    new GetPondList().execute(customerID);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void populateCustomerDetails(String customerID) {
        for (CustomerList c : customerList) {
            if (customerID.equalsIgnoreCase(c.getId())) {
                edit_customer.setText(c.getFirst_name());
            }
        }
    }

    private void initialize(View view) {
        edit_customer = view.findViewById(R.id.edit_customer_name);
        rc_pond_list = view.findViewById(R.id.rc_pond_list);
        fab_pond_create = view.findViewById(R.id.fab_pond_create);
    }

    @Override
    public boolean onBackPressed() {
        callHomeFragment();
        return true;
    }

    private void callHomeFragment() {
        Intent myIntent = new Intent(getActivity(), Home_Page_Activity.class);
        getActivity().startActivity(myIntent);
    }

    @Override
    public void onPondClick(int position, String id, String cycle_id) {
        PondDetailedListFragment fragment = new PondDetailedListFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            Bundle bundle = new Bundle();
            bundle.putString("POND_ID", id);
            bundle.putString("CYCLE_ID", cycle_id);
            AppLog.write("POND", "--" + id + "--" + cycle_id);
            fragment.setArguments(bundle);
            fragmentTransaction.commit();
        }
    }

    private class GetCustomerList extends AsyncTask<Void, Void, ArrayList<CustomerList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<CustomerList> doInBackground(Void... v) {
            ArrayList<CustomerList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCustomerMasterList((App) getActivity().getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<CustomerList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            customer_list_spinner.clear();
            for (CustomerList c_list : list) {
                customerList.add(c_list);
                customer_list_spinner.add(c_list.getFirst_name());
            }
        }
    }

    private class GetPondList extends AsyncTask<String, String, ArrayList<PondList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<PondList> doInBackground(String... v) {
            ArrayList<PondList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getPONDName((App) getActivity().getApplication(), customerID);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<PondList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                pondList.clear();
                for (PondList pond : list) {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(pond));
                    pondList.add(pond);
                }
                adapter.MyDataChanged(pondList);
            } else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
