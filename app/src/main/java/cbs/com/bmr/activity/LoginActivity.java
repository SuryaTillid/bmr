package cbs.com.bmr.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.SuccessMessage;

/*********************************************************************
 * Created by Barani on 02-04-2019 in TableMateNew
 ***********************************************************************/
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText ed_UserName;
    private EditText ed_Password;
    private Button btn_login;
    private String userName, passWord;
    private String employeeID, employeeNAME;
    private Context context;
    private TextView text_label;
    private Toolbar mToolbar;
    private String DayIN = "0";
    private ConfigurationSettings configurationSettings;
    private ArrayList<String> employee_spinner_list = new ArrayList<>();
    private ArrayList<EmployeeList> employeeList = new ArrayList<>();

    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = LoginActivity.this;
        configurationSettings = new ConfigurationSettings(context);
       /* String IpAddress = "https://bmr.codebase.bz";
        App.IP_ADDRESS = IpAddress;
        configurationSettings.setIP_ADDRESS(IpAddress);*/

        mToolbar = findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ed_UserName = findViewById(R.id.ed_userName);
        ed_Password = findViewById(R.id.ed_password);
        text_label = findViewById(R.id.text_label);
        btn_login = findViewById(R.id.btnLogin);
        btn_login.setOnClickListener(this);

        new LoadEmployeeRecords().execute();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btnLogin) {
            userName = ed_UserName.getText().toString().trim();
            passWord = ed_Password.getText().toString().trim();
            if (validate()) {
                if (userName != null && passWord != null) {
                    new validate_login().execute(userName, passWord);
                }
            }
        }
    }

    private boolean validate() {
        userName = ed_UserName.getText().toString().trim();
        passWord = ed_Password.getText().toString().trim();
        if (userName.equals("")) {
            ed_UserName.setError("Enter Username");
            ed_UserName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(passWord)) {
            ed_Password.setError("Enter Password");
            ed_Password.requestFocus();
            return false;
        }
        return true;
    }

    private void getRegionId() {
        for (EmployeeList e : employeeList) {
            if (configurationSettings.getEmployee_ID().equalsIgnoreCase(e.getId())) {
                AppLog.write("REGION--", "-" + e.getRegion_id());
                configurationSettings.setREGION_ID(e.getRegion_id());
            }
        }
    }

    @Override
    public void onBackPressed() {
        confirmExit();
    }

    private void confirmExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //exit_app();
                Intent intent = new Intent(LoginActivity.this, ConfigurationActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //noinspection SimplifiableIfStatement
        int id = item.getItemId();
        if(id == R.id.action_change_password)
        {
            //change_password();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void change_password() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_password_change);

        final EditText e_name = dialog.findViewById(R.id.ed_userName);
        final EditText e_new_password = dialog.findViewById(R.id.ed_new_password);
        Button btn_change = dialog.findViewById(R.id.b_change_pwd);

        e_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSpinnerForEmployeeSelection(e_name);
            }
        });

        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_password = e_new_password.getText().toString().trim();
                if (validatePwd(e_name, e_new_password)) {
                    if (new_password.length() < 8 && !isValidPassword(new_password)) {
                        Toast.makeText(context, "Invalid Password", Toast.LENGTH_SHORT).show();
                    } else {
                        AppLog.write("Credits--", "--" + new_password + "--" + employeeID);
                        new ChangePassword().execute(employeeID, new_password);
                        dialog.cancel();
                    }
                }
            }
        });
        dialog.show();
    }

    private boolean validatePwd(EditText e_name, EditText e_new_password) {
        String e_passWord = e_new_password.getText().toString().trim();
        String e_user = e_name.getText().toString().trim();
        if (e_user.equals("")) {
            e_name.setError("Enter Username");
            e_name.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(e_passWord)) {
            e_new_password.setError("Enter Password");
            e_new_password.requestFocus();
            return false;
        }
        return true;
    }

    private void callSpinnerForEmployeeSelection(final EditText e_name) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_employee_list_spinner);

        final Spinner spinner_employee_list = dialog.findViewById(R.id.employee_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, employee_spinner_list);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_employee_list.setAdapter(ad);
        spinner_employee_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                employeeID = employeeList.get(position).getId();
                employeeNAME = employeeList.get(position).getFirstname();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(employeeID)) {
                    populateEmployeeDetail(employeeID, e_name);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void populateEmployeeDetail(String employeeID, EditText e_name) {
        for (EmployeeList e : employeeList) {
            if (employeeID.equalsIgnoreCase(e.getId())) {
                e_name.setText(e.getFirstname());
            }
        }
    }

    private void exit_app() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    private class validate_login extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.LoginSubmit((App) getApplication(), p[0], p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != success) {
                AppLog.write("Login output-------", "-" + new Gson().toJson(success) + "--" + userName);
                String s = success.getSuccess();
                if (!TextUtils.isEmpty(s)) {
                    switch (s) {
                        case "2":
                            Toast.makeText(context, "Incorrect Username..!", Toast.LENGTH_SHORT).show();
                            break;
                        case "3":
                            Toast.makeText(context, "Incorrect Password..!", Toast.LENGTH_SHORT).show();
                            break;
                        case "4":
                            Toast.makeText(context, "Incorrect Username and Password..!", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            if (result != "-1") {
                                configurationSettings.setUSER_NAME(userName);
                                configurationSettings.setEmployee_ID(success.getE_id());
                                configurationSettings.setPASSWORD(passWord);
                                if (!TextUtils.isEmpty(configurationSettings.getEmployee_ID())) {
                                    getRegionId();
                                    new GetDayInStatus().execute();
                                }
                                Toast.makeText(context, "Login Succeed...", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Login Failed...", Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                } else {
                    Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class ChangePassword extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Creating Task...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_ChangePassword((App) getApplication(), p[0], p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Password successfully updated..!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class LoadEmployeeRecords extends AsyncTask<Void, Void, ArrayList<EmployeeList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeList> doInBackground(Void... v) {
            ArrayList<EmployeeList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getEmployeeList((App) getApplication());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                for (EmployeeList e_list : list) {
                    employeeList.add(e_list);
                }
            }
        }
    }

    private class GetDayInStatus extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_getDayIn_status((App) getApplication(), configurationSettings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Day_IN_status", "--" + new Gson().toJson(success));
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    DayIN = "1";
                    configurationSettings.setIS_CHECKED_IN(true);
                    configurationSettings.setUPDATE_ID(success.getUpdate_id());
                    configurationSettings.setCHECKED_IN_TIME(success.getCheckin_time());
                    Intent intent = new Intent(LoginActivity.this, Home_Page_Activity.class);
                    startActivity(intent);
                } else if (success.getSuccess().equalsIgnoreCase("2")) {
                    DayIN = "2";
                    Intent intent = new Intent(LoginActivity.this, Home_Page_Activity.class);
                    startActivity(intent);
                } else if (success.getSuccess().equalsIgnoreCase("0")) {
                    DayIN = "0";
                    Intent intent = new Intent(LoginActivity.this, Home_Page_Activity.class);
                    startActivity(intent);
                }
            } else {
                AppLog.write("Day_IN_status", "Not Working");
            }
        }
    }
}