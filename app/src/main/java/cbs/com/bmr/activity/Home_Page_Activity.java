package cbs.com.bmr.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;

/*********************************************************************
 * Created by Barani on 16-08-2019 in TableMateNew
 ***********************************************************************/
public class Home_Page_Activity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int REQUEST_ENABLE_LOCATION = 4;
    private static final long INTERVAL = 1000 * 600;
    private static final long FASTEST_INTERVAL = 1000 * 300;
    public Double lLat, lLng;
    Location mLastLocation, location;
    Window window;
    private ImageButton img_change_password, img_current_day_task, img_task_list, img_task_schedule, img_pond_create, img_sampling;
    private TextView t_check_in_time, t_in_time, t_hrs, t_out;
    private LinearLayout layout_fragment;
    private Toolbar mToolbar;
    private Context context;
    private boolean isInFront;
    private ConfigurationSettings settings;
    private Button btn_day_in, btn_day_out;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String Cur_latitude, Cur_longitude;
    private long t_stamp = System.currentTimeMillis();
    private Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        context = Home_Page_Activity.this;
        settings = new ConfigurationSettings(context);

        initialize();
        initAPIcalls();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
    }

    private void initAPIcalls() {
        initLocationRequest();
        buildGoogleApiClient();
        displayLocationSettingsRequest();
        new GetDayInStatus().execute();
    }

    private void initialize() {
        mToolbar = findViewById(R.id.toolbar);
        btn_day_in = findViewById(R.id.btn_day_in);
        btn_day_out = findViewById(R.id.btn_day_out);
        img_change_password = findViewById(R.id.img_change_password);
        img_current_day_task = findViewById(R.id.img_current_task);
        img_task_list = findViewById(R.id.img_task_list);
        img_task_schedule = findViewById(R.id.img_task_schedule);
        img_pond_create = findViewById(R.id.img_pond_create);
        img_sampling = findViewById(R.id.img_sampling);
        layout_fragment = findViewById(R.id.fragment_layout);
        t_check_in_time = findViewById(R.id.t_check_in_time);
        t_in_time = findViewById(R.id.t_in_time);
        t_out = findViewById(R.id.t_out_time);
        t_hrs = findViewById(R.id.t_hrs);
        img_change_password.setOnClickListener(this);
        img_current_day_task.setOnClickListener(this);
        img_task_list.setOnClickListener(this);
        img_task_schedule.setOnClickListener(this);
        img_pond_create.setOnClickListener(this);
        img_sampling.setOnClickListener(this);
        btn_day_out.setOnClickListener(this);
        btn_day_in.setOnClickListener(this);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void displayLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder =
                new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest).setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(Home_Page_Activity.this, REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @SuppressWarnings("deprecation")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        lLat = location.getLatitude();
        lLng = location.getLongitude();
        getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 4) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                }
            }
        }
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Cur_latitude = String.valueOf(latitude);
            Cur_longitude = String.valueOf(longitude);
            String lat_lon = new Gson().toJson(getLocationDetails(Cur_latitude, Cur_longitude));
            AppLog.write("LL------", lat_lon);
            String time_value = getTime();
            new SendUpdatedLocation().execute(time_value, lat_lon);
        }
    }

    private String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String s = sdf.format(timestamp);
        return s;
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        isInFront = false;
    }

    @Override
    public void onBackPressed() {
        //checkCurrentActivity();
        //AppLog.write("BACK_PRESSED","--"+isInFront);
        callFragments();
    }

    private void callFragments() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        boolean handled = false;
        for (Fragment f : fragments) {

            if (f instanceof TaskSchedulerFragment)
                handled = ((TaskSchedulerFragment) f).onBackPressed();
            if (f instanceof TaskUpdateFragment)
                handled = ((TaskUpdateFragment) f).onBackPressed();
            if (f instanceof AttendanceFragment)
                handled = ((AttendanceFragment) f).onBackPressed();
            if (f instanceof CurrentDateTaskFragment)
                handled = ((CurrentDateTaskFragment) f).onBackPressed();
            if (f instanceof CreateTaskForTodayFragment)
                handled = ((CreateTaskForTodayFragment) f).onBackPressed();
            if (f instanceof TaskListFragment)
                handled = ((TaskListFragment) f).onBackPressed();
            if (f instanceof FeedbackFromCustomer)
                handled = ((FeedbackFromCustomer) f).onBackPressed();
            if (f instanceof TaskUpdateFragmentForCurrentDayFragment)
                handled = ((TaskUpdateFragmentForCurrentDayFragment) f).onBackPressed();
            if (f instanceof ToDoList_Fragment)
                handled = ((ToDoList_Fragment) f).onBackPressed();
            if (f instanceof ManagementRequest)
                handled = ((ManagementRequest) f).onBackPressed();
            if (f instanceof ChangePasswordFragment)
                handled = ((ChangePasswordFragment) f).onBackPressed();
            if (f instanceof ManagementActivity_TaskCreate)
                handled = ((ManagementActivity_TaskCreate) f).onBackPressed();
            if (f instanceof ManagementCreateTaskForTodayFragment)
                handled = ((ManagementCreateTaskForTodayFragment) f).onBackPressed();
            if (f instanceof ManagementActivity_TaskApproval)
                handled = ((ManagementActivity_TaskApproval) f).onBackPressed();
            if (f instanceof PondCreationFragment)
                handled = ((PondCreationFragment) f).onBackPressed();
            if (f instanceof PondListFragment)
                handled = ((PondListFragment) f).onBackPressed();
            if (f instanceof SamplingDataList)
                handled = ((SamplingDataList) f).onBackPressed();
            if (f instanceof PondUpdateFragment)
                handled = ((PondUpdateFragment) f).onBackPressed();
            if (handled) {
                break;
            }
        }
        if (!handled) {
            confirmExit();
        }
    }

    private void confirmExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit, your session will be cleared?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(context, LoginActivity.class);
                startActivity(i);
                settings.clearSession();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        layout_fragment.setVisibility(View.VISIBLE);
        switch (v.getId()) {
            case R.id.btn_day_in:
                fragment = new AttendanceFragment();
                title = getString(R.string.title_attendance);
                mToolbar.setBackgroundColor(Color.parseColor("#e60000"));
                setStatusBarColor(0);
                break;
            case R.id.btn_day_out:
                fragment = new AttendanceFragment();
                title = getString(R.string.title_attendance);
                mToolbar.setBackgroundColor(Color.parseColor("#e60000"));
                setStatusBarColor(0);
                break;
            case R.id.img_change_password:
                fragment = new ChangePasswordFragment();
                title = getString(R.string.title_change_password);
                mToolbar.setBackgroundColor(Color.parseColor("#ffa366"));
                setStatusBarColor(1);
                break;
            case R.id.img_current_task:
                CurrentDateTaskFragment fragment_c = new CurrentDateTaskFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.container_body, fragment_c).commit();
                title = getString(R.string.title_today_task);
                getSupportActionBar().setTitle(title);
                mToolbar.setBackgroundColor(Color.parseColor("#43d0bf"));
                setStatusBarColor(4);
                break;
            case R.id.img_task_list:
                fragment = new TaskListFragment();
                title = getString(R.string.title_home);
                mToolbar.setBackgroundColor(Color.parseColor("#9771f0"));
                setStatusBarColor(5);
                break;
            case R.id.img_task_schedule:
                fragment = new TaskSchedulerFragment();
                title = getString(R.string.title_task_scheduler);
                mToolbar.setBackgroundColor(Color.parseColor("#6699ff"));
                setStatusBarColor(3);
                break;
            case R.id.img_pond_create:
                fragment = new PondListFragment();
                title = getString(R.string.title_pond_create);
                mToolbar.setBackgroundColor(Color.parseColor("#6699ff"));
                setStatusBarColor(3);
                break;
            case R.id.img_sampling:
                fragment = new SamplingDataList();
                title = getString(R.string.title_sampling);
                mToolbar.setBackgroundColor(Color.parseColor("#009933"));
                setStatusBarColor(6);
                break;
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }

    private String date_validate(String task_date) {
        try {
            if (!TextUtils.isEmpty(task_date)) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(task_date);
                DateFormat date_formatter = new SimpleDateFormat("hh:mm a");
                task_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return task_date;
    }

    private void setStatusBarColor(int code) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            switch (code) {
                case 0:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s0));
                    break;
                case 1:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s1));
                    break;
                case 2:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s2));
                    break;
                case 3:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s3));
                    break;
                case 4:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s4));
                    break;
                case 5:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s5));
                    break;
                case 6:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s6));
                    break;
                case 7:
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.s7));
                    break;
            }
    }

    private class GetDayInStatus extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_getDayIn_status((App) getApplication(), settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Executed_Day_IN_status", "-" + new Gson().toJson(success));
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    AppLog.write("Check_In", "----");
                    btn_day_out.setVisibility(View.VISIBLE);
                    btn_day_in.setVisibility(View.GONE);
                    t_check_in_time.setText(date_validate(settings.getCHECKED_IN_TIME()));
                    t_in_time.setText(date_validate(settings.getCHECKED_IN_TIME()));

                } else if (success.getSuccess().equalsIgnoreCase("2")) {
                    AppLog.write("Not_Checked_In", "----");
                    btn_day_out.setVisibility(View.GONE);
                    btn_day_in.setVisibility(View.VISIBLE);
                    t_check_in_time.setText("00:00");
                }
            }
        }
    }

    /*
     * Set Color for tool bar
     * */

    private class SendUpdatedLocation extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.postEmployeeGPSLog((App) getApplication(), settings.getEmployee_ID(), p[0], p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage successMessage) {
            super.onPostExecute(successMessage);
            dialog.dismiss();
            AppLog.write("Response", "---" + new Gson().toJson(successMessage));
        }
    }
}