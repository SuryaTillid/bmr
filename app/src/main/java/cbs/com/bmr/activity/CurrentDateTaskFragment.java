package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.TaskListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TaskList;

/********************************************************************
 * Created by Barani on 27-03-2019 in TableMateNew
 ********************************************************************/
public class CurrentDateTaskFragment extends Fragment implements TaskDetails_ClickListener, View.OnClickListener, LocationListener, On_BackPressed {

    LocationManager locationManager;
    ConfigurationSettings settings;
    private TaskListAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recyclerView_task;
    private ArrayList<TaskList> taskList = new ArrayList<>();
    private Context context;
    private String current_date, approval_status;
    private FloatingActionButton fab_task_create;
    private TextView text_date;
    private String check_in_out, task_id, lat_lon, check_date, s_e_date;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_task_for_today, container, false);

        context = getActivity();
        settings = new ConfigurationSettings(context);

        recyclerView_task = view.findViewById(R.id.rc_TaskDetailsList);
        fab_task_create = view.findViewById(R.id.fab_task_create);
        text_date = view.findViewById(R.id.text_date);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        fab_task_create.setOnClickListener(this);

        //get date
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time=>" + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat d_format = new SimpleDateFormat("dd-MMM-yyyy");
        s_e_date = df.format(c);
        current_date = df.format(c);

        text_date.setText(d_format.format(c));

        adapter = new TaskListAdapter(context, taskList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setTaskListListener(this);
        recyclerView_task.setLayoutManager(mLayoutManager);
        recyclerView_task.setAdapter(adapter);

        //call Tasks List
        new GetTaskList().execute();
        getLocation();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refresh();
                stop();
            }
        });
        return view;
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Home_Page_Activity) getActivity()).setActionBarTitle("Task for Today");
    }

    @Override
    public void onTaskClick(int position, String id, String check) {
        check_in_out = check;
        task_id = id;
        getLocation();
        if (!TextUtils.isEmpty(lat_lon)) {
            if (check_in_out.equalsIgnoreCase("1")) {
                new GetCheck_In_Details().execute(task_id, check_date, lat_lon);
            } else {
                new GetCheck_Out_Details().execute(task_id, check_date, lat_lon);
            }
        } else {
            Toast.makeText(context, "Location not detected, please try again..!", Toast.LENGTH_SHORT).show();
            getLocation();
        }
    }

    @Override
    public void feedbackSubmit(String id, String type) {
        Bundle bundle = new Bundle();
        bundle.putString("ID", id);
        bundle.putString("TYPE", type);
        FeedbackFromCustomer fragment = new FeedbackFromCustomer();
        fragment.setArguments(bundle);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClickEdit(int position, String t_id, String task_id) {
        Bundle bundle = new Bundle();
        bundle.putString("T_ID", t_id);
        bundle.putString("TASK_ID", task_id);
        bundle.putString("DATE_C", s_e_date);
        TaskUpdateFragmentForCurrentDayFragment fragment = new TaskUpdateFragmentForCurrentDayFragment();
        fragment.setArguments(bundle);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClickApproval(int position, final String t_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Task Approval Status");
        builder.setPositiveButton("Approve", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "1";
                new TaskApprovalCheck().execute(t_id, "1");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "2";
                new TaskApprovalCheck().execute(t_id, "2");
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void callFragment() {
        TaskListFragment fragment = new TaskListFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClick(View v) {
        CreateTaskForTodayFragment fragment = new CreateTaskForTodayFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            String lat = String.valueOf(location.getLatitude());
            String lon = String.valueOf(location.getLongitude());
            if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lon)) {
                lat_lon = new Gson().toJson(getLocationDetails(lat, lon));
                AppLog.write("LAT_LON", lat_lon);
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                check_date = date_format.format(calendar.getTime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    private void callCheck_in_out(String lat_lon, String check_date, String check_in_out, String task_id) {
        if (check_in_out.equalsIgnoreCase("1")) {
            new GetCheck_In_Details().execute(task_id, check_date, lat_lon);
        } else {
            new GetCheck_Out_Details().execute(task_id, check_date, lat_lon);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public boolean onBackPressed() {
        //callFragment();
        callHomeActivity();
        return true;
    }

    private void callHomeActivity() {
        Intent myIntent = new Intent(getActivity(), Home_Page_Activity.class);
        getActivity().startActivity(myIntent);
    }

    private void refresh() {
        new GetTaskList().execute();
    }

    private class GetTaskList extends AsyncTask<Void, Void, ArrayList<TaskList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TaskList> doInBackground(Void... v) {
            final String url = App.IP_ADDRESS + "/index.php/tasksummary/getlisttaskapi";
            AppLog.write("URL:::", "-" + url);
            ArrayList<TaskList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getTaskDetailsList((App) getActivity().getApplication(), settings.getEmployee_ID(), settings.getREGION_ID(), s_e_date, s_e_date);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<TaskList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();

            ArrayList<TaskList> tList = new ArrayList<>();
            if (null != list) {
                AppLog.write("T_date_list", "--" + new Gson().toJson(list));
                for (TaskList task : list) {
                    AppLog.write("T_date", "--" + task.getTask_date() + "--" + current_date);
                    String task_date = task.getTask_date();
                    try {
                        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        Date date = format.parse(task_date);
                        DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy");
                        AppLog.write("T_date_", "--" + task_date);
                        task_date = date_formatter.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (task_date.equalsIgnoreCase(current_date)) {
                        tList.add(task);
                    }
                }
                adapter.MyDataChanged(tList);
            } else {
                Toast.makeText(context, "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /* private String taskDateFormat(String task_date) {
         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
         Date myDate = null;
         try {
             myDate = dateFormat.parse(task_date);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
         String finalDate = timeFormat.format(myDate);
         return finalDate;
     }
 */
    private class GetCheck_In_Details extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckIn_Task_update((App) getActivity().getApplication(), p[0], p[1], p[2], settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("CHECK_IN", "--" + new Gson().toJson(success));
            if (success != null) {
                if (success.getCheckin_status().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Check-in Successfully..!", Toast.LENGTH_SHORT).show();
                    refresh();
                } else {
                    Toast.makeText(getActivity(), "Failed to Check-in..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-in..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetCheck_Out_Details extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckOut_Task_update((App) getActivity().getApplication(), p[0], p[1], p[2], settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("CHECK__OUT", "---" + new Gson().toJson(success));
            if (success != null) {
                if (success.getCheckout_status().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Check-out Successfully..!", Toast.LENGTH_SHORT).show();
                    refresh();
                } else {
                    Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class TaskApprovalCheck extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckApprovedStatus((App) getActivity().getApplication(), settings.getEmployee_ID(), p[0], p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (approval_status.equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Approved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Cancelled", Toast.LENGTH_SHORT).show();
                }
                refresh();
            }
        }
    }
}