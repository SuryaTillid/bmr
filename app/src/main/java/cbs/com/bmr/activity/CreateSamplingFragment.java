package cbs.com.bmr.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.SuccessMessage;

/********************************************************************
 * Created by Barani on 21-08-2019 in TableMateNew
 ********************************************************************/
public class CreateSamplingFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private ConfigurationSettings settings;
    private String emp_id, cycle_id, harvest_flag = "0", recorded_date;
    private Button btn_create_sampling, btn_active, btn_harvest;
    private EditText edit_abw, edit_daily_feed;

    public CreateSamplingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_create_sampling, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);
        emp_id = settings.getEmployee_ID();

        initialize(view);

        Bundle bundle = this.getArguments();
        cycle_id = bundle.getString("CYCLE_ID");

        btn_create_sampling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                recorded_date = date_format.format(c.getTime());
                String abw = edit_abw.getText().toString().trim();
                String daily_feed = edit_daily_feed.getText().toString().trim();
                new CreateSampling_Details().execute(cycle_id, emp_id, daily_feed, recorded_date, harvest_flag, abw);
            }
        });

        return view;
    }

    private void initialize(View v) {
        btn_create_sampling = v.findViewById(R.id.btn_sample_create);
        btn_active = v.findViewById(R.id.btn_active);
        btn_harvest = v.findViewById(R.id.btn_harvest);
        edit_daily_feed = v.findViewById(R.id.edit_daily_feed);
        edit_abw = v.findViewById(R.id.edit_abw);
        btn_active.setOnClickListener(this);
        btn_harvest.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_active:
                harvest_flag = "0";
                changeColor(btn_active);
                break;
            case R.id.btn_in_active:
                harvest_flag = "1";
                changeColor(btn_harvest);
                break;
        }
    }

    private void changeColor(Button selected_btn) {
        btn_active.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_harvest.setBackgroundResource(R.drawable.grey_hollow_round_button);

        selected_btn.setTextColor(Color.parseColor("#4e66f5"));
        selected_btn.setTextColor(Color.parseColor("#4e66f5"));
    }

    private void callHomeFragment() {
    }

    private class CreateSampling_Details extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Creating Task...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.createSamplingRecord((App) getActivity().getApplication(), s[0], s[1], s[2], s[3], s[4], s[5]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Sample Created..!", Toast.LENGTH_SHORT).show();
                    callHomeFragment();
                } else {
                    Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
