package cbs.com.bmr.activity;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cbs.com.bmr.R;

/*********************************************************************
 * Created by Barani on 17-06-2019 in TableMateNew
 ***********************************************************************/
public class TaskTodayActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = TaskTodayActivity.this;

        Fragment fragment = new CurrentDateTaskFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        String title = getString(R.string.title_today_task);
        // getSupportActionBar().setTitle(title);
        fragmentTransaction.replace(R.id.container_body, fragment, title);
        fragmentTransaction.commit();
    }
}
