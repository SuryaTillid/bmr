package cbs.com.bmr.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.EmployeeInfoForManagementRequest;
import cbs.com.bmr.model.ManagementRequestList;
import cbs.com.bmr.model.SuccessMessage;

/********************************************************************
 * Created by Barani on 20-05-2019 in TableMateNew
 ********************************************************************/

public class ManagementRequestUpdate extends Fragment implements On_BackPressed, View.OnClickListener {

    private Context context;
    private ConfigurationSettings settings;
    private String s_department, s_req_by, s_req_qty, s_expected_qty, s_description, s_comments, emp_id, emp_department;
    private EditText edit_department, edit_request_by, edit_request_qty, edit_expected_qty, edit_desc, edit_comment;
    private Button btn_submit;
    private String task_id_edit;
    private ArrayList<ManagementRequestList> m_list = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_management_request_create, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);
        initialize(view);

        task_id_edit = this.getArguments().getString("TASK_R_ID");

        new GetEmployeeInfoList().execute(settings.getEmployee_ID());

        new GetRequestList().execute();

        return view;
    }

    private void initialize(View view) {
        edit_department = view.findViewById(R.id.edit_department);
        edit_request_by = view.findViewById(R.id.edit_request_by);
        edit_request_qty = view.findViewById(R.id.edit_request_qty);
        edit_expected_qty = view.findViewById(R.id.edit_expected_budget);
        edit_desc = view.findViewById(R.id.edit_description);
        edit_comment = view.findViewById(R.id.edit_comments);
        btn_submit = view.findViewById(R.id.btn_create_request);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public boolean onBackPressed() {
        callHomeFragment();
        return false;
    }

    private void callHomeFragment() {
        ManagementRequest fragment = new ManagementRequest();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClick(View v) {
        if (validate()) {
            s_description = edit_desc.getText().toString().trim();
            s_comments = edit_comment.getText().toString().trim();
            s_req_by = edit_request_by.getText().toString().trim();
            s_department = edit_department.getText().toString().trim();
            s_expected_qty = edit_expected_qty.getText().toString().trim();
            new UpdateManagementRequest().execute(task_id_edit, emp_id, emp_department, s_description, s_comments, "0", s_expected_qty);
        }
    }

    private boolean validate() {
        boolean isValid = true;

        if (edit_desc.getText().toString().equals("")) {
            edit_desc.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_desc.setError(null);
            s_description = edit_desc.getText().toString().trim();
        }

        if (edit_expected_qty.getText().toString().equals("")) {
            edit_expected_qty.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_expected_qty.setError(null);
            s_expected_qty = edit_expected_qty.getText().toString().trim();
        }
        return isValid;
    }

    private void updateValues(ArrayList<ManagementRequestList> m_list) {
        for (ManagementRequestList t : m_list) {
            edit_department.setText(t.getDepartment_name());
            edit_request_by.setText(t.getReq_by());
            edit_expected_qty.setText(t.getExpected_budget());
            edit_desc.setText(t.getReq_desc());
            edit_comment.setText(t.getComments());
        }
    }

    private class UpdateManagementRequest extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.managementRequestUpdate((App) getActivity().getApplication(), s[0], s[1], s[2], s[3], s[4], s[5], s[6]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Successfully Updated..!", Toast.LENGTH_SHORT).show();
                    callHomeFragment();
                } else if (success.getSuccess().equalsIgnoreCase("2")) {
                    Toast.makeText(getActivity(), "No changes have been made..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Failed to Update..!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class GetEmployeeInfoList extends AsyncTask<String, String, EmployeeInfoForManagementRequest> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected EmployeeInfoForManagementRequest doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.getEmployeeInfoForRequestList((App) getActivity().getApplication(), settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(EmployeeInfoForManagementRequest list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Employee_list", "--" + new Gson().toJson(list));
            edit_department.setText(list.getDepartment_name());
            edit_request_by.setText(list.getEmployee_name());
            emp_id = list.getId();
            emp_department = list.getEmp_dep();
        }
    }

    private class GetRequestList extends AsyncTask<Void, Void, ArrayList<ManagementRequestList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<ManagementRequestList> doInBackground(Void... v) {
            ArrayList<ManagementRequestList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getManagementRequestList((App) getActivity().getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<ManagementRequestList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Request_list", "--" + new Gson().toJson(list));
            if (null != list) {
                m_list.clear();
                for (ManagementRequestList m : list) {
                    if (m.getId().equalsIgnoreCase(task_id_edit)) {
                        AppLog.write("Checking 1.", "---------" + new Gson().toJson(m));
                        m_list.add(m);
                    }
                }
                updateValues(m_list);
            } else {
                Toast.makeText(getActivity(), "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}