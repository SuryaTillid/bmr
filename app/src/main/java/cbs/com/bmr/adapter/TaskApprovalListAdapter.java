package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.model.TaskList;

/*********************************************************************
 * Created by Barani on 26-06-2019 in TableMateNew
 ***********************************************************************/
public class TaskApprovalListAdapter extends RecyclerView.Adapter<TaskApprovalListAdapter.MyHolder> {
    private ArrayList<TaskList> taskLists;
    private Context context;
    private LayoutInflater inflater;
    private TaskDetails_ClickListener listener;

    public TaskApprovalListAdapter(Context context, ArrayList<TaskList> taskList) {
        this.context = context;
        this.taskLists = taskList;
        inflater = LayoutInflater.from(context);
    }

    public void setTaskListListener(TaskDetails_ClickListener locationListener) {
        this.listener = locationListener;
    }

    public void MyDataChanged(ArrayList<TaskList> list) {
        taskLists = list;
        notifyDataSetChanged();
    }

    @Override
    public TaskApprovalListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_task_approval_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(TaskApprovalListAdapter.MyHolder holder, int position) {
        TaskList taskList = taskLists.get(position);

        holder.t_customer_name.setText(taskList.getCustomername());
        holder.t_date.setText(taskList.getTask_date());
        holder.t_type.setText(type_validate(taskList.getTask_type()));
        holder.t_approve.setText(approveValidate(taskList.getApproved_status()));
    }

    private String approveValidate(String approved_status) {
        switch (approved_status) {
            case "0":
                approved_status = "Pending";
                break;
            case "1":
                approved_status = "Approved";
                break;
            case "2":
                approved_status = "Cancelled";
                break;
        }
        return approved_status;
    }

    private String type_validate(String task_type) {
        switch (task_type) {
            case "0":
                task_type = "General";
                break;
            case "1":
                task_type = "Collection";
                break;
            case "2":
                task_type = "Services";
                break;
            case "3":
                task_type = "Sales";
                break;
        }
        return task_type;
    }

    @Override
    public int getItemCount() {
        return taskLists.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView t_date;
        TextView t_approve;
        TextView t_purpose, t_customer_name;
        TextView t_type;

        public MyHolder(View v) {
            super(v);
            t_customer_name = v.findViewById(R.id.t_customer_name);
            t_date = v.findViewById(R.id.t_task_date);
            t_type = v.findViewById(R.id.txtType);
            t_approve = v.findViewById(R.id.t_approval);

            t_approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickApproval(getAdapterPosition(), taskLists.get(getAdapterPosition()).getId());
                }
            });
        }

        private String check_for_type(String task_type) {
            String s = "";
            switch (task_type) {
                case "General":
                    s = "1";
                    break;
                case "Collection":
                    s = "2";
                    break;
                case "Service":
                    s = "3";
                    break;
                case "Sales":
                    s = "4";
                    break;
            }
            return s;
        }
    }
}
