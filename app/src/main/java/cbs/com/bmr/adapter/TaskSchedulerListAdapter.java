package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.model.TaskScheduler;

/*********************************************************************
 * Created by Barani on 27-03-2019 in TableMateNew
 ***********************************************************************/
public class TaskSchedulerListAdapter extends RecyclerView.Adapter<TaskSchedulerListAdapter.MyHolder> {
    private ArrayList<TaskScheduler> taskLists;
    private Context context;
    private LayoutInflater inflater;
    private TaskDetails_ClickListener listener;

    public TaskSchedulerListAdapter(Context context, ArrayList<TaskScheduler> taskList) {
        this.context = context;
        this.taskLists = taskList;
        inflater = LayoutInflater.from(context);
    }

    public void setTaskListListener(TaskDetails_ClickListener locationListener) {
        this.listener = locationListener;
    }

    public void MyDataChanged(ArrayList<TaskScheduler> list) {
        taskLists = list;
        notifyDataSetChanged();
    }

    @Override
    public TaskSchedulerListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_task_list, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(TaskSchedulerListAdapter.MyHolder holder, int position) {
        TaskScheduler taskList = taskLists.get(position);

        holder.t_date.setText(taskList.getTask_date());
        holder.t_notes.setText(taskList.getDescription());
        holder.t_customer_name.setText(taskList.getCustomer_name());
        holder.t_location_address.setText(taskList.getLocation());
    }

    @Override
    public int getItemCount() {
        return taskLists.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView t_location_address;
        TextView t_notes, t_lat, t_lon, t_date;
        TextView t_purpose, t_customer_name;
        TextView t_edit;

        public MyHolder(View v) {
            super(v);

            t_lat = v.findViewById(R.id.txtLatitude);
            t_lon = v.findViewById(R.id.txtLongitude);
            t_location_address = v.findViewById(R.id.txtAddr);
            t_notes = v.findViewById(R.id.txtNotes);
            t_customer_name = v.findViewById(R.id.t_customer_name);
            t_date = v.findViewById(R.id.t_task_date);
            t_edit = v.findViewById(R.id.t_edit);

            t_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onTaskClick(getAdapterPosition(), taskLists.get(getAdapterPosition()).getT_id(), "");
                }
            });
        }
    }
}