package cbs.com.bmr.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import cbs.com.bmr.R;
import cbs.com.bmr.model.CustomerList;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public class CustomerNameAdapter extends ArrayAdapter<CustomerList> {

    Context context;
    private int resourceID, textViewResourceId;
    private ArrayList<CustomerList> customerList, cLIST, listItems;
    private Filter customerFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((CustomerList) resultValue).getFirst_name();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                customerList.clear();
                for (CustomerList c_list : cLIST) {
                    if (c_list.getFirst_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        customerList.add(c_list);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = customerList;
                filterResults.count = customerList.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<CustomerList> customerLIST = (ArrayList<CustomerList>) results.values;
            if (results.count > 0) {
                clear();
                addAll(customerLIST);
                notifyDataSetChanged();
            }
        }
    };

    public CustomerNameAdapter(@NonNull Context context, int resourceID, int textViewResourceId, @NonNull ArrayList<CustomerList> c_list) {
        super(context, resourceID, textViewResourceId, c_list);
        this.listItems = c_list;
        this.context = context;
        this.resourceID = resourceID;
        this.textViewResourceId = textViewResourceId;
        cLIST = new ArrayList<>(listItems);
        customerList = new ArrayList<>();
    }

    @Nullable
    @Override
    public CustomerList getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return customerFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                view = inflater.inflate(R.layout.activity_customer_auto_text_row, parent, false);
            }
            CustomerList cList = listItems.get(position);
            TextView name = view.findViewById(R.id.textView_customerName);
            name.setText(cList.getFirst_name());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
}
