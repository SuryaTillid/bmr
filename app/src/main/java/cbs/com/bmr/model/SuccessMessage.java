package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class SuccessMessage {
    private String success;
    private String val;
    private String id;
    private String checkout_status;
    private String checkin_status;
    private int feedback_status;
    private String e_id;
    private String update_id;
    private String checkin_time;
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCheckout_status() {
        return checkout_status;
    }

    public void setCheckout_status(String checkout_status) {
        this.checkout_status = checkout_status;
    }

    public String getCheckin_status() {
        return checkin_status;
    }

    public void setCheckin_status(String checkin_status) {
        this.checkin_status = checkin_status;
    }

    public int getFeedback_status() {
        return feedback_status;
    }

    public void setFeedback_status(int feedback_status) {
        this.feedback_status = feedback_status;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getE_id() {
        return e_id;
    }

    public void setE_id(String e_id) {
        this.e_id = e_id;
    }

    public String getUpdate_id() {
        return update_id;
    }

    public void setUpdate_id(String update_id) {
        this.update_id = update_id;
    }

    public String getCheckin_time() {
        return checkin_time;
    }

    public void setCheckin_time(String checkin_time) {
        this.checkin_time = checkin_time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
